angular.module('LiveEngine.Support.Services', [])

.service('SupportServices', function(GlobalVariables, LoggerService, FileSystemService, SocketService, $rootScope, $http, $ionicModal, $ionicPopup, LanguageService) {

    var me = this;
    me.HTTP_TIMEOUT = 15000; //era 2000
    me.BUFFER_DIRECTORY = "bufferDirectory" + GlobalVariables.directory_trail;

    /*
    me.getLabel = function(codice, lingua) {

        //override della lingua
        lingua=GlobalVariables.systemLanguage;

        var returnValue = "no-value";

        var token = _.find(GlobalVariables.systemLabels, function(item) { return item.codice == codice; });

         if(token) {
            returnValue = token[lingua] ? token[lingua] : token[lingua + 'Default'];
        }

        return returnValue;

    };

*/


    me.getGuideImageLinks = function(parameters) {

        var BUFFER_FILE_NAME = "guida_" + GlobalVariables.idCliente + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveDataGetConfig.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              action: 'getGuideData',
              idCliente: GlobalVariables.idCliente
            }
        })
        .then(function(json) {

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: JSON.stringify(json.data),
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        /* alert("config saving failed: " + err); */
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        parameters.onComplete("ERRORE - TESTO IN FUNZIONE CHIAMANTE", null);
                    } else {
                        parameters.onComplete(null, JSON.parse(readData));
                    }

                }
            });

        });

    };



    me.getLibrariesToLoad = function(parameters) {

      var BUFFER_FILE_NAME = "libs_testata_" + parameters.idTestata + ".json";

      $http({
          url: GlobalVariables.application.applicationUrl + "/getLibraries",
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {

          FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: JSON.stringify(json.data),
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      /* alert("config saving failed: " + err); */
                  }

                  parameters.onComplete(null, json.data);

              }
          });

      }, function(error, xhr) {

          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      parameters.onComplete(null, JSON.parse(readData));
                  }

              }
          });

      });

    };

    me.getARLibrariesToLoad = function(parameters) {

      var BUFFER_FILE_NAME = "libsAR_testata_" + parameters.idTestata + ".json";

      console.log("sono in getARLibrariesToLoad, con BUFFER_FILE_NAME: " + BUFFER_FILE_NAME);
      console.log("url: " + GlobalVariables.application.applicationUrl + "/getARLibraries");


      $http({
          url: GlobalVariables.application.applicationUrl + "/getARLibraries",
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {

          console.log("sono nel Then");
          FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: JSON.stringify(json.data),
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      /* alert("config saving failed: " + err); */
                  }

                  parameters.onComplete(null, json.data);

              }
          });

      }, function(error, xhr) {

          console.log("sono nell'error");

          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  //console.log("onComplete del fileRead, con dir: " + me.BUFFER_DIRECTORY + " e filename: " + BUFFER_FILE_NAME + " e err: " + err + " e readData: " + readData);
                  if(err || !readData) {
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      parameters.onComplete(null, JSON.parse(readData));
                  }

              }
          });

      });

    };


    me.getUsersByPrjId = function(parameters) {
      
      //console.log(GlobalVariables.application.applicationUrl + "/usersCountByPrjId");

      $http({
          url: GlobalVariables.application.applicationUrl + "/usersCountByPrjId",
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {
          
          parameters.onComplete(null, json.data);

      }, function(error, xhr) {

          parameters.onComplete("ERRORE - TESTO NON IMPORTANTE, NON VERRA MOSTRATO", null);

      });

    };

    me.getUsersBySocket = function(parameters) {
      
      $http({
          url: parameters.url + "/usersCount",
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {
          
          parameters.onComplete(null, json.data);

      }, function(error, xhr) {

          parameters.onComplete("ERRORE - TESTO NON IMPORTANTE, NON VERRA MOSTRATO", null);

      });

    };

    me.sendPushId2Server = function(parameters) {
      
      var serverUrl=parameters.url+"/"+parameters.packageName+"/"+parameters.uid+"/"+parameters.pushId+"/setPushId";
      console.log("Spedisco al server: " + serverUrl);
      
      $http({
          url: serverUrl,
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {
          
          parameters.onComplete(null, json.data);

      }, function(error, xhr) {

          parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);

      });

    };



    me.getLayoutTestata = function(parameters) {

        var BUFFER_FILE_NAME = "layout_testata_" + parameters.idTestata + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveDataLayoutTestata.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idTestata: parameters.idTestata
            }
        })
        .then(function(json) {

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: JSON.stringify(json.data),
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        /* alert("config saving failed: " + err); */
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        parameters.onComplete(null, JSON.parse(readData));
                    }

                }
            });

        });

    };






    me.getConfiguration = function(parameters) {

        var BUFFER_FILE_NAME = "configuration.json";

        $http({

            url: GlobalVariables.baseUrl + "/liveDataSystemMetadataCliente.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idCliente: GlobalVariables.idCliente
            }

        }).then(function(json) {

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: JSON.stringify(json.data),
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        /* alert("config saving failed: " + err); */
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

          console.log("getConfiguration - error: " + JSON.stringify(error));

            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        parameters.onComplete(null, JSON.parse(readData));
                    }

                }
            });


        });

    };



    // questo metodo restituisce i percorsi per i poi del cliente
    me.getPercorsi = function(parameters) {

        var BUFFER_FILE_NAME = "percorsi.json";

        $http({

            url: GlobalVariables.baseUrl + "/liveDataPercorsi.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idCliente: GlobalVariables.idCliente,
              uid: GlobalVariables.deviceUUID,
              appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            }

        }).then(function(json) {

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: JSON.stringify(json.data),
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        // alert("config saving failed: " + err);
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        parameters.onComplete(null, JSON.parse(readData));
                    }

                }
            });

        });

    }

    // questo metodo restituisce i poi del cliente, da usare per vedere se si devono scaricare HS custom
    me.getPoi = function(parameters) {

        var BUFFER_FILE_NAME = "poi.json";

        $http({

            url: GlobalVariables.baseUrl + "/liveDataPoi.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idPercorso: parameters.idPercorso,
              idCliente: GlobalVariables.idCliente,
              flagHS: "true"
            }

        }).then(function(json) {

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: JSON.stringify(json.data),
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        // alert("config saving failed: " + err);
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        parameters.onComplete(null, JSON.parse(readData));
                    }

                }
            });

        });

    }


    // questo metodo restituisce le copertine
    me.getListaCopertine = function(parameters) {

        var BUFFER_FILE_NAME = "copertine.json";

        $http({

            url: GlobalVariables.baseUrl + "/liveDataProgettiTestata.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idCliente: GlobalVariables.idCliente,
              uid: GlobalVariables.deviceUUID,
              appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            }

        }).then(function(json) {

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: JSON.stringify(json.data),
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        // alert("config saving failed: " + err);
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        parameters.onComplete(null, JSON.parse(readData));
                    }

                }
            });

        });

    }



    me.getNumeriTestata = function(parameters) {

        var BUFFER_FILE_NAME = "numeri_testata_" + parameters.idTestata + "_" + parameters.start + "_" + parameters.limit + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveDataCopertineTestata.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                idTestata: parameters.idTestata,
                anno: (parameters.anno ? parameters.anno : ""),
                start: (parameters.start ? parameters.start : ""),
                limit: (parameters.limit ? parameters.limit : ""),
                uid: GlobalVariables.deviceUUID
            }
        })
        .then(function(json) {

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: JSON.stringify(json.data),
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        // alert("config saving failed: " + err);
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      parameters.onComplete(null, JSON.parse(readData));
                  }

              }
          });

        });

    };


    me.getDettaglioProgetto = function(parameters) {

        var BUFFER_FILE_NAME = "dettaglio_progetto_" + parameters.idProgetto + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveDataNew.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                idProgetto: parameters.idProgetto,
                uid: GlobalVariables.deviceUUID,
                appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            }
        })
        .then(function(json) {

          FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: JSON.stringify(json.data),
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      // alert("config saving failed: " + err);
                  }

                  parameters.onComplete(null, json.data);

              }
          });

        }, function(error, xhr) {

          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      parameters.onComplete(null, JSON.parse(readData));
                  }

              }
          });

        });

    };

    
    me.sceltaProgetto = function(parameters) {

      console.log("scelta progetto");
      me.getDettaglioProgetto({
          idProgetto: GlobalVariables.application.currentProgetto.idProgetto,
          onComplete: function(err, json) {

              
              console.log("eccolo");
              if(err) {

                  var alertPopup = $ionicPopup.alert({
                     title: LanguageService.getLabel('ATTENZIONE'),
                      template: LanguageService.getLabel('PROBLEMI_RETE')
                   });
                  parameters.onComplete("TESTO NON IMPORTANTE, VERRA USATO QUELLO DEL CHIAMANTE", null); 
              }

              GlobalVariables.application.currentProgetto.detail = json.progetti[0];
              GlobalVariables.application.currentProgetto.gradimento = json.gradimento;

              for(var j=0; j<GlobalVariables.application.currentProgetto.detail.target.length; j++) {

                  GlobalVariables.application.currentProgetto.detail.target[ j ].imagePath =
                    GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.detail.target[ j ].imagePath;

              }


              // ################################ //
              // evento 'apertura di una rivista' //
              // ################################ //
              SocketService.emit('message','has_view_progetto', {}, {
                  uid: GlobalVariables.deviceUUID,
                  id: GlobalVariables.application.currentProgetto.idProgetto,
                  description: GlobalVariables.application.currentProgetto.detail.prjDescr,
                  name: GlobalVariables.application.currentProgetto.detail.prjName
              });
              // ################################ //
              // evento 'apertura di una rivista' //
              // ################################ //

              //ritorno
              parameters.onComplete(null, null); 
              
          }

      });


    };


})
