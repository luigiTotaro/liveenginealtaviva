angular.module('LiveEngine.Wiki.Services', [])

.service('WikiService', function(LoggerService, HelperService, $rootScope, $http, $ionicModal, $ionicPopup, $ionicLoading, GlobalVariables, FileSystemService, SupportServices, LazyLoader, LanguageService, $state) {

    var me = this;

    me.toDownload = new Array();

    me.callbacks = [];


    me.registerCallback = function(key, functionName, callback) {

        if(!me.callbacks[key]) {
            me.callbacks[key] = [];
        }

        delete me.callbacks[key][functionName];

        if(callback) {
            me.callbacks[key][functionName] = callback;
        }

    }

    me.setupCordova = function ()
    {

      if(typeof window.cordova == "undefined") return true;

      try{
        GlobalVariables.wikitudePlugin = cordova.require("com.wikitude.phonegap.WikitudePlugin.WikitudePlugin");
        return true;
      }
      catch(err){
        console.log(err);
        return false;

      }
    };

    me.test = function() {
      console.log("Satizza");
    };


    me.sendMessageToAR = function(json) {
        var jsonString = JSON.stringify(json);
        //console.log("sendMessageToAR: " + jsonString);
        jsonString = jsonString.replace(/'/g, "|");   

        var encodedJson = encodeURIComponent(jsonString);
        var funzione = "messageForAR('" + encodedJson + "')";
        //console.log("funzione: " + funzione);

        GlobalVariables.wikitudePlugin.callJavaScript(funzione);
    };


    me.parseWikitudeCallback = function(url)
    {
        var ret = new Object();
        ret.value="";
        
        console.log("parseWikitudeCallback: "+ url);
        var parsedUrl= encodeURI(url.substring(15));
        if (parsedUrl.substring(0,9)=="copertina")
        {
            ret.value="copertina";    
            ret.parameter = parsedUrl.substring(9);
        }
        else if (parsedUrl.substring(0,9)=="nonRilevo")
        {
            ret.value="nonRilevo";    
        }
        else if (parsedUrl.substring(0,7)=="storage")
        {
            ret.value="storage";    
        }
        else if (parsedUrl.substring(0,15)=="scaricaImmagine")
        {
            ret.value="scaricaImmagine";    
            var value=parsedUrl.substring(16);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        }
        else if (parsedUrl.substring(0,11)=="salvaConfig")
        {
            ret.value="salvaConfig";    
            var value=parsedUrl.substring(12);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        }
        else if (parsedUrl.substring(0,11)=="linkEsterno")
        {
            ret.value="linkEsterno";    
            var value=parsedUrl.substring(12);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=value;
        }
        else if (parsedUrl.substring(0,16)=="checkConnessione")
        {
            ret.value="checkConnessione";    
        }
        else if (parsedUrl.substring(0,14)=="getUsersNumber")
        {
            ret.value="getUsersNumber";    
        }
        else if (parsedUrl.substring(0,16)=="scaricaSlideshow")
        {
            ret.value="scaricaSlideshow";    
            var value=parsedUrl.substring(17);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        }
        else if (parsedUrl.substring(0,14)=="existSlideshow")
        {
            ret.value="existSlideshow";    
            ret.parameter=parsedUrl.substring(15);
        }
        else if (parsedUrl.substring(0,11)=="openContent")
        {
            ret.value="openContent";    
            var value=parsedUrl.substring(12);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        }
        else if (parsedUrl.substring(0,13)=="socialPublish")
        {
            ret.value="socialPublish";    
            var value=parsedUrl.substring(14);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        }
        else if (parsedUrl.substring(0,12)=="writeComment")
        {
            ret.value="writeComment";    
            var value=parsedUrl.substring(13);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        }        
        else if (parsedUrl.substring(0,6)=="naviga")
        {
            ret.value="naviga";    
            var value=parsedUrl.substring(7);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        }

        return ret;
    }


    me.loadARchitectWorldCopertine = function (example)
    {
        console.log("Entrato in loadARchitectWorldCopertine");
        console.log(JSON.stringify(example));
        // check if the current device is able to launch ARchitect Worlds

        GlobalVariables.wikitudePlugin.requestAccess(function()
        {
            console.log("Request Access OK");



            GlobalVariables.wikitudePlugin.isDeviceSupported(function()
            {
             	console.log("Device is supported");
               //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);

                GlobalVariables.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
                {
                    //console.log("loadARworld ok, call calljavascript: "+example.localPath);
                    /* Respond to successful world loading if you need to */

                    console.log("dovrei averla chiamata....");

                }, function errorFn(error)
                {
                    console.log("Loading AR web view failed" + error);
                    //alert("Errore nell'avvio della Realtà Aumentata. Contattare l'assistenza");
                    HelperService.showUnrecoverableError(LanguageService.getLabel('ERRORE_AVVIO_RA','it'), true);
               },
                example.path, example.requiredFeatures, example.startupConfiguration
                );
                GlobalVariables.wikitudePlugin.callJavaScript('passData("'+example.localPath+'","'+example.wtc+'","'+example.baseUrl+'","'+example.numUtenti+'","'+example.colore+'")');
                GlobalVariables.wikitudePlugin.setOnUrlInvokeCallback(function (url)
        	        {
                        var parse = me.parseWikitudeCallback(url);
                        switch(parse.value) {
                            case "copertina":
                                console.log("Ha riconosciuto la copertina del progetto: "+ parse.parameter);
                                GlobalVariables.wikitudePlugin.close();
                                GlobalVariables.application.currentProgetto = {
                                    idProgetto: parse.parameter
                                };

                                $ionicLoading.show({
                                  template: LanguageService.getLabel('ATTENDERE','it')
                                });

                                SupportServices.sceltaProgetto({
                                  onComplete: function(err, json) {

                                      $ionicLoading.hide();
                                      if(err) {
                                          var alertPopup = $ionicPopup.alert({
                                             title: 'Attenzione',
                                             template: 'Sembrano esserci problemi di rete che impediscono la fruizione dell\'applicazione'
                                           });
                                            $state.go('navigazione_progetti');
                                      }
                                      $state.go('progetto_detail');
                                  }
                                });
                                break;
                            case "checkConnessione":
                                var connessione=HelperService.isNetworkAvailable();
                                console.log("Check connessione: " + connessione);
                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessione('+connessione+')');
                                break;
                            case "nonRilevo":
                                GlobalVariables.wikitudePlugin.close();
                                $state.go('navigazione_progetti');
                                break;
                             default:
                                GlobalVariables.wikitudePlugin.close();
                                $state.go('navigazione_progetti');
                        }
        	    	});

            }, function(errorMessage)
            {
            	console.log("Device is NOT supported");
                //alert("Device is NOT supported");
                HelperService.showUnrecoverableError(LanguageService.getLabel('DEVICE_NON_SUPPORTATO','it'), true);
            },
            example.requiredFeatures
            );


        }, function(errorMessage)
        {
            console.log(errorMessage);
            //alert("Non riesco ad accedere alla Fotocamera");
            HelperService.showUnrecoverableError(LanguageService.getLabel('NO_ACCESSO_FOTOCAMERA','it'), true);
        },
        example.requiredFeatures
        );


    }



    me.loadARchitectWorldProgetto = function (example)
    {


        console.log("Entrato in loadARchitectWorld");

        GlobalVariables.wikitudePlugin.requestAccess(function()
        {
            console.log("Request Access OK");


            // check if the current device is able to launch ARchitect Worlds
            GlobalVariables.wikitudePlugin.isDeviceSupported(function()
            {
                console.log("Device is supported");
               //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);


                GlobalVariables.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
                {
                    //console.log("loadARworld ok, call calljavascript: "+example.localPath);
                    /* Respond to successful world loading if you need to */

                    console.log("dovrei averla chiamata....");

                }, function errorFn(error)
                {
                    console.log("Loading AR web view failed");
                    //alert('Loading AR web view failed');
                    HelperService.showUnrecoverableError(LanguageService.getLabel('ERRORE_AVVIO_RA','it'), true);
                },
                example.path, example.requiredFeatures, example.startupConfiguration
                );
                GlobalVariables.wikitudePlugin.callJavaScript('passData("'+example.localPath+'","'+example.localDynamicARPath+'","'+example.idPrj+'",'+example.isApiRest+',"'+example.baseUrl+'","'+example.deviceId+'",'+LazyLoader.appConfiguration.showDisclaimerForComments+',"'+LazyLoader.appConfiguration.commentsNickname+'","'+example.multiTarget+'")');
                GlobalVariables.wikitudePlugin.setOnUrlInvokeCallback(function (url)
                    {
                        var parse = me.parseWikitudeCallback(url);
                        switch(parse.value) {
                            case "storage":
                                break;
                            case "scaricaImmagine":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.TRIGGER_WIKI_EVT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        type: 'download_item',
                                        url: parse.parameter.url,
                                        descr: parse.parameter.descr
                                    }
                                });
                                break;
                            case "salvaConfig":
                                LazyLoader.appConfiguration.showDisclaimerForComments = parse.parameter.showCommentsDisclaimer;
                                LazyLoader.appConfiguration.commentsNickname = parse.parameter.commentsNickname;
                                LazyLoader.saveConfiguration({
                                    onComplete: function(err) {
                                    }
                                })
                                break;
                            case "linkEsterno":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_EXTERNAL_URL,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        url: parse.parameter
                                    }
                                });
                                break;
                            case "checkConnessione":
                                var connessione=HelperService.isNetworkAvailable();
                                console.log("Check connessione: " + connessione);
                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessione('+connessione+')');
                                break;
                            case "getUsersNumber":
                                var numero=GlobalVariables.application.currentProgetto.numUtenti;
                                GlobalVariables.wikitudePlugin.callJavaScript('getUsersNumber('+numero+')');
                                break;
                            case "scaricaSlideshow":
                                //nell'oggetto ci sta l'id del punto e le immagini da scaricare
                                me.toDownload = new Array();
                                var idPunto=parse.parameter[0];
                                for (var index=1;index<parse.parameter.length;index++)
                                {
                                  me.toDownload.push(parse.parameter[index]);
                                }
                                console.log("array da scaricare");
                                console.log(me.toDownload);
                                me.downloadFiles(0,idPunto);
                                break;
                            case "existSlideshow":
                                //esiste, nella directory slideshow, qualche file che inizia con l'id del punto?
                                var name=parse.parameter+"_0"; //almeno il primo
                                FileSystemService.fileExists({
                                    directory: SupportServices.BUFFER_DIRECTORY + '/slideshow',
                                    fileName: name,
                                    onComplete: function(err, isAvailable) {
                                        if(err) {
                                            //errore, getto la spugna
                                            console.log("Errore, comunico che non ci sono files");
                                            GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                        } else {
                                            if(!isAvailable) {
                                                 //non esiste, getto la spugna
                                                console.log("Non Esiste, comunico che non ci sono files");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                            } else {
                                                //esiste, vado avanti
                                                console.log("Esiste");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(true)');
                                            }
                                        }
                                    }
                                });
                                break;
                            case "openContent":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_AUGMENTED_CONTENT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        id: parse.parameter.id,
                                        titolo: parse.parameter.titolo
                                    }
                                });
                                break;
                            case "socialPublish":
                                var oggetto=parse.parameter;

                                GlobalVariables.wikitudePlugin.hide();
                                
                                $ionicLoading.show({
                                    template: '<div style="padding:10px 5px;">Attendere.....</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
                                    duration: 15000
                                });

                                var appParameter="";

                                if (oggetto.type=="fb")
                                {
                                    
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.facebook";
                                    else appParameter="com.facebook.katana";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per fb');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                           window.plugins.socialsharing.shareViaFacebook(null, oggetto.image, oggetto.link, function()
                                            {
                                                console.log('share ok fb');
                                                me.showPopupReturn2AR("fb");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore fb: ' + errormsg);
                                                me.showPopupReturn2AR("fb");
                                            });
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            me.showPopupReturn2AR("fb_error");
                                            $ionicLoading.hide();
                                        });
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="tw")
                                {
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.twitter";
                                    else appParameter="twitter";

                                     setTimeout(function() {
                                        console.log('chiamo il plugin di share per tw');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaTwitter(oggetto.text, oggetto.image, "", function()
                                            {
                                                console.log('share ok tw');
                                                me.showPopupReturn2AR("tw");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore tw: ' + errormsg);
                                                me.showPopupReturn2AR("tw");
                                            });  
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            me.showPopupReturn2AR("tw_error");
                                            $ionicLoading.hide();
                                        });
                                        console.log("dovrei aver fatto");
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="insta")
                                {

                                    if (GlobalVariables.platform=="iOS") appParameter="instagram";
                                    else appParameter="instagram";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per instagram');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaInstagram(oggetto.text, oggetto.image, function()
                                            {
                                              console.log('share ok insta');
                                                me.showPopupReturn2AR("insta");
                                                //GlobalVariables.wikitudePlugin.show();
                                            }, function(errormsg)
                                            {
                                              console.log('Errore insta: ' + errormsg);
                                                me.showPopupReturn2AR("insta");
                                                 //GlobalVariables.wikitudePlugin.show();
                                           });                               
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            me.showPopupReturn2AR("insta_error");
                                            $ionicLoading.hide();
                                        });
                                     }, 1000);                              
                                }
                                break;
                            case "writeComment":
                                GlobalVariables.wikitudePlugin.hide();
                                me.showPopupWriteComment(parse.parameter);
                                break;
                            default:
                                console.log("Esco dalla RA");
                                try{
                                    GlobalVariables.wikitudePlugin.close();
                                    console.log("dovrei averla chiusa");
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                catch(err){
                                    console.log(err);
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                        }
                    });

            }, function(errorMessage)
            {
                console.log("Device is NOT supported");
                //alert(errorMessage);
                HelperService.showUnrecoverableError(LanguageService.getLabel('DEVICE_NON_SUPPORTATO','it'), true);
            },
            example.requiredFeatures
            );

        }, function(errorMessage)
        {
            console.log(errorMessage);
            //alert("Non riesco ad accedere alla Fotocamera");
            HelperService.showUnrecoverableError(LanguageService.getLabel('NO_ACCESSO_FOTOCAMERA','it'), true);
        },
        example.requiredFeatures
        );



    }

    me.loadARchitectWorldPercorso = function (example)
    {


        console.log("Entrato in loadARchitectWorld Percorso");

        GlobalVariables.wikitudePlugin.requestAccess(function()
        {
            console.log("Request Access OK");


            // check if the current device is able to launch ARchitect Worlds
            GlobalVariables.wikitudePlugin.isDeviceSupported(function()
            {
                console.log("Device is supported");
               //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);


                GlobalVariables.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
                {
                    //console.log("loadARworld ok, call calljavascript: "+example.localPath);
                    /* Respond to successful world loading if you need to */

                    console.log("dovrei averla chiamata....");

                }, function errorFn(error)
                {
                    console.log("Loading AR web view failed");
                    //alert('Loading AR web view failed');
                    HelperService.showUnrecoverableError(LanguageService.getLabel('ERRORE_AVVIO_RA','it'), true);
                },
                example.path, example.requiredFeatures, example.startupConfiguration
                );
                GlobalVariables.wikitudePlugin.callJavaScript('passData("'+example.localPath+'","'+example.localDynamicARPath+'","'+example.idPercorso+'",'+example.idCliente+',"'+example.baseUrl+'","'+example.deviceId+'",'+LazyLoader.appConfiguration.showDisclaimerForComments+',"'+LazyLoader.appConfiguration.commentsNickname+'",'+example.position.coords.latitude+','+example.position.coords.longitude+','+example.position.coords.altitude+')');
                GlobalVariables.wikitudePlugin.setOnUrlInvokeCallback(function (url)
                    {
                        var parse = me.parseWikitudeCallback(url);
                        switch(parse.value) {
                            case "storage":
                                break;
                            case "scaricaImmagine":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.TRIGGER_WIKI_EVT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        type: 'download_item',
                                        url: parse.parameter.url,
                                        descr: parse.parameter.descr
                                    }
                                });
                                break;
                            case "salvaConfig":
                                LazyLoader.appConfiguration.showDisclaimerForComments = parse.parameter.showCommentsDisclaimer;
                                LazyLoader.appConfiguration.commentsNickname = parse.parameter.commentsNickname;
                                LazyLoader.saveConfiguration({
                                    onComplete: function(err) {
                                    }
                                })
                                break;
                            case "linkEsterno":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_EXTERNAL_URL,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        url: parse.parameter
                                    }
                                });
                                break;
                            case "checkConnessione":
                                var connessione=HelperService.isNetworkAvailable();
                                console.log("Check connessione: " + connessione);
                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessione('+connessione+')');
                                break;
                            case "getUsersNumber":
                                var numero=GlobalVariables.application.currentProgetto.numUtenti;
                                GlobalVariables.wikitudePlugin.callJavaScript('getUsersNumber('+numero+')');
                                break;
                            case "scaricaSlideshow":
                                //nell'oggetto ci sta l'id del punto e le immagini da scaricare
                                me.toDownload = new Array();
                                var idPunto=parse.parameter[0];
                                for (var index=1;index<parse.parameter.length;index++)
                                {
                                  me.toDownload.push(parse.parameter[index]);
                                }
                                console.log("array da scaricare");
                                console.log(me.toDownload);
                                me.downloadFiles(0,idPunto);
                                break;
                            case "existSlideshow":
                                //esiste, nella directory slideshow, qualche file che inizia con l'id del punto?
                                var name=parse.parameter+"_0"; //almeno il primo
                                FileSystemService.fileExists({
                                    directory: SupportServices.BUFFER_DIRECTORY + '/slideshow',
                                    fileName: name,
                                    onComplete: function(err, isAvailable) {
                                        if(err) {
                                            //errore, getto la spugna
                                            console.log("Errore, comunico che non ci sono files");
                                            GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                        } else {
                                            if(!isAvailable) {
                                                 //non esiste, getto la spugna
                                                console.log("Non Esiste, comunico che non ci sono files");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                            } else {
                                                //esiste, vado avanti
                                                console.log("Esiste");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(true)');
                                            }
                                        }
                                    }
                                });
                                break;
                            case "openContent":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_AUGMENTED_CONTENT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        id: parse.parameter.id,
                                        titolo: parse.parameter.titolo
                                    }
                                });
                                break;
                            case "naviga":
                                if ( device.platform == 'android' || device.platform == 'Android' || device.platform == "amazon-fireos" ){
                                    // Google Maps (Android)
                                    var ref = window.open('http://maps.google.com/maps?q=' + parse.parameter.lat + '+' + parse.parameter.lon, '_system', 'location=yes');
                                } else {
                                    // Apple
                                    window.location.href = 'maps://maps.apple.com/?q=' + parse.parameter.lat + '+' + parse.parameter.lon;
                                }
                                break;
                            case "socialPublish":
                                var oggetto=parse.parameter;

                                GlobalVariables.wikitudePlugin.hide();
                                
                                $ionicLoading.show({
                                    template: '<div style="padding:10px 5px;">Attendere.....</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
                                    duration: 15000
                                });

                                var appParameter="";

                                if (oggetto.type=="fb")
                                {
                                    
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.facebook";
                                    else appParameter="com.facebook.katana";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per fb');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                           window.plugins.socialsharing.shareViaFacebook(null, oggetto.image, oggetto.link, function()
                                            {
                                                console.log('share ok fb');
                                                me.showPopupReturn2AR("fb");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore fb: ' + errormsg);
                                                me.showPopupReturn2AR("fb");
                                            });
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            me.showPopupReturn2AR("fb_error");
                                            $ionicLoading.hide();
                                        });
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="tw")
                                {
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.twitter";
                                    else appParameter="twitter";

                                     setTimeout(function() {
                                        console.log('chiamo il plugin di share per tw');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaTwitter(oggetto.text, oggetto.image, "", function()
                                            {
                                                console.log('share ok tw');
                                                me.showPopupReturn2AR("tw");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore tw: ' + errormsg);
                                                me.showPopupReturn2AR("tw");
                                            });  
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            me.showPopupReturn2AR("tw_error");
                                            $ionicLoading.hide();
                                        });
                                        console.log("dovrei aver fatto");
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="insta")
                                {

                                    if (GlobalVariables.platform=="iOS") appParameter="instagram";
                                    else appParameter="instagram";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per instagram');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaInstagram(oggetto.text, oggetto.image, function()
                                            {
                                              console.log('share ok insta');
                                                me.showPopupReturn2AR("insta");
                                                //GlobalVariables.wikitudePlugin.show();
                                            }, function(errormsg)
                                            {
                                              console.log('Errore insta: ' + errormsg);
                                                me.showPopupReturn2AR("insta");
                                                 //GlobalVariables.wikitudePlugin.show();
                                           });                               
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            me.showPopupReturn2AR("insta_error");
                                            $ionicLoading.hide();
                                        });
                                     }, 1000);                              
                                }
                                break;
                            case "writeComment":
                                GlobalVariables.wikitudePlugin.hide();
                                me.showPopupWriteComment(parse.parameter);
                                break;
                            default:
                                console.log("Esco dalla RA");
                                try{
                                    GlobalVariables.wikitudePlugin.close();
                                    console.log("dovrei averla chiusa");
                                    $state.transitionTo('poi_detail', null, {reload: true, notify:true});
                                }
                                catch(err){
                                    console.log(err);
                                    $state.transitionTo('poi_detail', null, {reload: true, notify:true});
                                }
                        }
                    });

            }, function(errorMessage)
            {
                console.log("Device is NOT supported");
                //alert(errorMessage);
                HelperService.showUnrecoverableError(LanguageService.getLabel('DEVICE_NON_SUPPORTATO','it'), true);
            },
            example.requiredFeatures
            );

        }, function(errorMessage)
        {
            console.log(errorMessage);
            //alert("Non riesco ad accedere alla Fotocamera");
            HelperService.showUnrecoverableError(LanguageService.getLabel('NO_ACCESSO_FOTOCAMERA','it'), true);
        },
        example.requiredFeatures
        );



    }




    me.loadARchitectWorldConfigurable = function (example)
    {
        console.log("Entrato in loadARchitectWorldConfigurable");
        console.log(JSON.stringify(example));
        // check if the current device is able to launch ARchitect Worlds

        GlobalVariables.wikitudePlugin.requestAccess(function()
        {
            console.log("Request Access OK");
            GlobalVariables.wikitudePlugin.isDeviceSupported(function()
            {
                console.log("Device is supported");
               //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);

                GlobalVariables.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
                {
                    //console.log("loadARworld ok, call calljavascript: "+example.localPath);
                    /* Respond to successful world loading if you need to */

                    console.log("dovrei averla chiamata....");

                }, function errorFn(error)
                {
                    console.log("Loading AR web view failed" + error);
                    //alert("Errore nell'avvio della Realtà Aumentata. Contattare l'assistenza");
                    HelperService.showUnrecoverableError(LanguageService.getLabel('ERRORE_AVVIO_RA','it'), true);
               },
                example.path, example.requiredFeatures, example.startupConfiguration
                );

                GlobalVariables.wikitudePlugin.callJavaScript("passData('"+example.localPath+"','"+example.wtc+"','"+example.game_key+"','"+example.labels+"')");    

                GlobalVariables.wikitudePlugin.setOnUrlInvokeCallback(function (url) {

                        console.log("url chiamata: "+ url);

                        var parsedUrl = url.substring(15);


                        if(parsedUrl.substring(0,13) == "messageFromAr") {

                            console.log("sono dentro messageFromAr...");

                            // elaboro un json con i parametri che vengono dalla AR
                            var msgString=parsedUrl.substring(13);
                            console.log("messaggio1: " + msgString);

                            msgString=decodeURIComponent(msgString);
                            console.log("messaggio2: " + msgString);

                            var msg = JSON.parse(msgString);
                            /*
                            var msg = {
                                type: 'tipo di messaggio',
                                msg_body: {
                                    // quello che vuoi
                                }
                            };
                            */

                            
                            if(msg.key) {
                                 
                                for(var functionName in me.callbacks[msg.key]) {
                                    me.callbacks[msg.key][functionName](msg);
                                }
                                
                            }


                        }

                        else if (parsedUrl.substring(0,9)=="startFind")
                        {
                            var value=parsedUrl.substring(10);
                            //alert(value);
                        }
                        else if (parsedUrl.substring(0,7)=="endFind")
                        {
                            var value=parsedUrl.substring(8);
                            //alert(value);
                        }
                        else if (parsedUrl.substring(0,11)=="abandon")
                        {
                            //alert("abandon");
                            GlobalVariables.wikitudePlugin.close();
                            $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                        }
                        else
                        {
                            //alert("uscita");
                            GlobalVariables.wikitudePlugin.close();
                            $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                        }

                    });

            }, function(errorMessage)
            {
                console.log("Device is NOT supported");
                //alert("Device is NOT supported");
                HelperService.showUnrecoverableError(LanguageService.getLabel('DEVICE_NON_SUPPORTATO','it'), true);
            },
            example.requiredFeatures
            );


        }, function(errorMessage)
        {
            console.log(errorMessage);
            //alert("Non riesco ad accedere alla Fotocamera");
            HelperService.showUnrecoverableError(LanguageService.getLabel('NO_ACCESSO_FOTOCAMERA','it'), true);
        },
        example.requiredFeatures
        );


    }


    me.go2ProjectPage = function() {
        $ionicLoading.show({
          template: LanguageService.getLabel('ATTENDERE','it')
        });

        SupportServices.sceltaProgetto({
          onComplete: function(err, json) {

              $ionicLoading.hide();
              if(err) {

                  var alertPopup = $ionicPopup.alert({
                     title: 'Attenzione',
                     template: '----Sembrano esserci problemi di rete che impediscono la fruizione dell\'applicazione'
                   });

                  return;
              }

              $state.go('progetto_detail');

          }

        });
    };


    me.downloadFiles = function(index,idPunto) {

      console.log("sono in downloadFiles, con index: " + index + " e idPunto: ", idPunto);

      var path = me.toDownload[index];
      var name = idPunto+"_"+index;
      var dir = "slideshow";

      var uri = encodeURI(path);

      var localpath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;


        console.log("Provo a scaricare " + uri);

        FileSystemService.downloadFile({
            url: uri,
            directory: SupportServices.BUFFER_DIRECTORY + '/' + dir,
            fileName: name,
            onComplete: function(err) {

                if(err) {
                    console.log("download error: " + err);
                    //provo comunque ad andare avanti
                    index++;
                    if (index<me.toDownload.length)
                    {
                    //console.log("index: " + index);
                    me.downloadFiles(index,idPunto);
                    }
                    else
                    {
                    console.log("Finito");
                    }


                } else {

                    console.log("download completato");
                    index++;
                    if (index<me.toDownload.length)
                    {
                    //console.log("index: " + index);
                    me.downloadFiles(index,idPunto);
                    }
                    else
                    {
                    console.log("Finito");
                    }

                }
            }
        });


  }

  me.openArForGame = function(game_key, dati)
  {
    var wtcName=dati.wtc;
    console.log("wtcName: " + wtcName);
    
    if (wtcName==null)
    {
        console.log("Attenzione, i contenuti per questo gioco non sono stati pubblicati. Riprovare in seguito, grazie");
        HelperService.showUnrecoverableError("Attenzione, i contenuti per questo gioco non sono stati pubblicati.<br>Riprovare in seguito, grazie", true);
        $ionicLoading.hide();
        return;
    }

    var labels="";
    if (typeof dati.labels == "undefined") labels="|||";
    else
    {
        if (typeof dati.labels.abbandona != "undefined") labels+=dati.labels.abbandona;
        labels+="|";
        if (typeof dati.labels.conferma_abbandona != "undefined") labels+=dati.labels.conferma_abbandona;
        labels+="|";
        if (typeof dati.labels.conferma_abbandona_si != "undefined") labels+=dati.labels.conferma_abbandona_si;
        labels+="|";
        if (typeof dati.labels.conferma_abbandona_no != "undefined") labels+=dati.labels.conferma_abbandona_no;
    }
    //debug
    //labels="vattene via|sei sicuro di volere abbandonare?ma proprio proprio??|si, me ne vado|no, rimango";

    var uri = encodeURI(GlobalVariables.baseUrl + "/"+wtcName);
    var name = wtcName.substring(16);
    var localpath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;
    var fullName=wtcName;

    var parameter = { "path": "www/clientRecognition/indexConfigurable.html", "requiredFeatures": ["image_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localpath, "wtc":fullName, "game_key": game_key, "labels": labels};

    //è presente la connessione?
    console.log("Check connessione: " + HelperService.isNetworkAvailable());
    if (HelperService.isNetworkAvailable()==false)
    {
    console.log("Nessuna connessione, vedo se il file è comunque presente");
    FileSystemService.fileExists({
        directory: SupportServices.BUFFER_DIRECTORY + '/wtcConfigurable',
        fileName: name,
        onComplete: function(err, isAvailable) {

            if(err) {
                //errore, getto la spugna
                console.log("Errore, esco");
                HelperService.showUnrecoverableError("Non riesco a scaricare i dati, possibile problema di connessione.<br>Riprovare in seguito.", true);

            } else {

                if(!isAvailable) {
                     //non esiste, getto la spugna
                    console.log("Non Esiste, esco");
                    HelperService.showUnrecoverableError("Non riesco a scaricare i dati, possibile problema di connessione.<br>Riprovare in seguito.", true);

                } else {
                    //esiste, vado avanti
                    console.log("Esiste, vado avanti");
                    console.log(JSON.stringify(parameter));
                    me.loadARchitectWorldConfigurable(parameter);
                }
            }
        }
    });
    }
    else
    {

    console.log("Provo a scaricare " + uri);

    FileSystemService.downloadFile({
        url: uri,
        directory: SupportServices.BUFFER_DIRECTORY + '/wtcConfigurable',
        fileName: name,
        onComplete: function(err) {

            if(err) {
              console.log("download error: " + err);
              console.log("Controllo se è comunque presente sul device");
              //non è riuscito, vedo se ho comunque il file sul device
              FileSystemService.fileExists({
                  directory: SupportServices.BUFFER_DIRECTORY + '/wtcConfigurable',
                  fileName: name,
                  onComplete: function(err, isAvailable) {

                      if(err) {
                          //errore, getto la spugna
                          console.log("Errore, esco");
                          HelperService.showUnrecoverableError("Non riesco a scaricare i dati, possibile problema di connessione.<br>Riprovare in seguito.", true);

                      } else {

                          if(!isAvailable) {
                               //non esiste, getto la spugna
                              console.log("Non Esiste, esco");
                              HelperService.showUnrecoverableError("Non riesco a scaricare i dati, possibile problema di connessione.<br>Riprovare in seguito.", true);

                          } else {
                              //esiste, vado avanti
                              console.log("Esiste, vado avanti");
                               console.log(JSON.stringify(parameter));
                              me.loadARchitectWorldConfigurable(parameter);
                          }
                      }
                  }
              });

            } else {

              console.log("download completato");
              console.log(JSON.stringify(parameter));
              me.loadARchitectWorldConfigurable(parameter);
            }
        }
    });
    }




  }


  me.showPopupReturn2AR = function(social) {

    $rootScope.closePopupScreen = function() {
        $rootScope.popupScreen.remove();
        GlobalVariables.wikitudePlugin.show();
    };

    $rootScope.popupScreen = $ionicModal.fromTemplate(


      '<ion-modal-view padding="true" cache-view="false" id="appCreditsScreen" style="width: 100%;height: 100%;top: 0px;left: 0px;">' +

          '<ion-content scroll="true" padding="false" >' +

              '<div id="logoSocialBox">' +

              (social == "fb" ? '<img src="img/fbLogo.png" style="width:100%;"></img>' : '') +
              (social == "tw" ? '<img src="img/twitterLogo.png" style="width:100%;"></img>' : '') +
              (social == "insta" ? '<img src="img/instagramLogo.png" style="width:100%;"></img>' : '') +
              
              (social == "insta_error" ? '<img src="img/instagramLogo.png" style="width:100%;"></img>' : '') +
              (social == "tw_error" ? '<img src="img/twitterLogo.png" style="width:100%;"></img>' : '') +
              (social == "fb_error" ? '<img src="img/fbLogo.png" style="width:100%;"></img>' : '') +

              '</div>'+

              '<div id="socialInfoBox" style="">' +
                  (social == "insta_error" ? 'Attenzione, la condivisione non è andata a buon fine. L\'app di Instagram è correttamente installata sul dispositivo?<br><br>' : '') +
                  (social == "tw_error" ? 'Attenzione, la condivisione non è andata a buon fine. L\'app di Twitter è correttamente installata sul dispositivo?<br><br>' : '') +
                  (social == "fb_error" ? 'Attenzione, la condivisione non è andata a buon fine. L\'app di Facebook è correttamente installata sul dispositivo?<br><br>' : '') +

                  '<strong>Premere il pulsante CHIUDI per tornare nella sessione di Realtà Aumentata.....</strong>' +

              '</div>'+

          '</ion-content>' +


          '<ion-footer-bar align-title="left" class="splashScreenFooter">' +
              '<button ng-click="closePopupScreen()" class="button button-full button-positive cst-button" style="background-color: #b41223;margin-top: auto;">CHIUDI</button>' +
          '</ion-footer-bar>' +

      '</ion-modal-view>',

      {
          scope: $rootScope,
          focusFirstInput: true,
          animation :'none',
          hardwareBackButtonClose: false
      }

  );

  $rootScope.popupScreen.show();


};


  me.showPopupWriteComment = function(oggetto) {

    $rootScope.comment_data = {
      tmp_nickname: oggetto.nickname,
      tmp_comment: "",
      show_warn_nickname: false,
      show_warn_comment: false,
      show_warn_nickname_comment: false
    };


    $rootScope.inviaCommento = function() {
        //controllo il nickname e il commento
        
        //tolgo i ritorni a capo
        var commento=$rootScope.comment_data.tmp_comment;
        console.log($rootScope.comment_data.tmp_comment);
        $rootScope.comment_data.tmp_comment = $rootScope.comment_data.tmp_comment.replace(/(\r\n|\n|\r)/gm," ");
        console.log($rootScope.comment_data.tmp_comment);

        var reg = /[^-A-Za-z0-9 .,!&:_@$^;|<>"'=+*()èéòàùì]/;
        var testNick = reg.test($rootScope.comment_data.tmp_nickname);    
        var testCommento = reg.test($rootScope.comment_data.tmp_comment);    
        
        $rootScope.comment_data.show_warn_nickname_comment=false;
        $rootScope.comment_data.show_warn_nickname=false;
        $rootScope.comment_data.show_warn_comment=false;

        if ((testNick==true) && (testCommento==true))
        {
            console.log("caratteri non ammessi nel nickname e nel commento");
            $rootScope.comment_data.show_warn_nickname_comment=true;
            setTimeout(function() {
                $rootScope.comment_data.show_warn_nickname_comment=false;
            }, 4000);       
          return;
        }        
        if (testNick==true)
        {
            console.log("caratteri non ammessi nel nickname");
            $rootScope.comment_data.show_warn_nickname=true;
            setTimeout(function() {
                $rootScope.comment_data.show_warn_nickname=false;
            }, 4000);       
          return;
        }        
        if (testCommento==true)
        {
            console.log("caratteri non ammessi nel commento");
            $rootScope.comment_data.show_warn_comment=true;
            setTimeout(function() {
                $rootScope.comment_data.show_warn_comment=false;
            }, 4000);       
          return;
        } 
        
        var return_oggetto = new Object();
        return_oggetto.type = "SEND";
        return_oggetto.nick = $rootScope.comment_data.tmp_nickname;
        return_oggetto.commento = $rootScope.comment_data.tmp_comment;

        $rootScope.popupScreen.remove();
        //alert($rootScope.comment_data.tmp_nickname);
        //alert($rootScope.comment_data.tmp_comment);

        var jsonString = JSON.stringify(return_oggetto);
        //tolgo eventuali apici, sostituendoli...
        jsonString = jsonString.replace(/'/g, "|");   

        var encodedJson = encodeURIComponent(jsonString);
        var funzione = "commentReturn('" + encodedJson + "')";
        //console.log("funzione: " + funzione);

        GlobalVariables.wikitudePlugin.show();
        GlobalVariables.wikitudePlugin.callJavaScript(funzione);
        
    };

    $rootScope.annullaCommento = function() {
        
        var return_oggetto = new Object();
        return_oggetto.type = "CANCEL";
        return_oggetto.nick = null;
        return_oggetto.commento = null;

        $rootScope.popupScreen.remove();

        var jsonString = JSON.stringify(return_oggetto);
        var encodedJson = encodeURIComponent(jsonString);
        var funzione = "commentReturn('" + encodedJson + "')";
        //console.log("funzione: " + funzione);

        GlobalVariables.wikitudePlugin.show();
        GlobalVariables.wikitudePlugin.callJavaScript(funzione);    
    };


    $rootScope.popupScreen = $ionicModal.fromTemplate(


      '<ion-modal-view padding="true" cache-view="false" id="appCommentScreen" style="width: 100%;height: 100%;top: 0px;left: 0px;">' +

          '<ion-content scroll="true" padding="false" >' +


            '<p style="color:#000000;text-align:left;margin-left:5%;">Inserisci un commento per '+oggetto.label+'</p>' + 
            (oggetto.nickname == "" ? '<p style="color:#000000;text-align:left;margin-left:5%;">Inserisci un nickName con il quale verrà pubblicato il tuo commento.</p>' : '') +
            '<p style="color:#000000;text-align:left;margin-left:5%;">Nickname</p>' + 
            '<input type="text" ng-model="comment_data.tmp_nickname" id="comment_nickname" maxlength="20" style="border:1px solid;color:#000000;text-align:left;margin-left:5%;width:50%;" placeholder="Inserisci il tuo nickname" value="'+oggetto.nickname+'">' + 
            '<p style="color:#000000;text-align:left;margin-left:5%;">Commento</p>' + 
            '<textarea rows="10" ng-model="comment_data.tmp_comment" style="color:#000000;text-align:left;margin-left:5%;width:90%;border:1px solid black;" placeholder="Inserisci il tuo commento" value=""></textarea>' +

            '<div ng-show="comment_data.show_warn_nickname" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>Attenzione nel nickname vi sono dei caratteri non permessi. Sono accettati solo i caratteri alfanumerici (A-Z a-z 0-9) e i seguenti simboli:  .,!&:_@$^;|<>\"\'=+*()èéòàùì</p></div>' +
            '<div ng-show="comment_data.show_warn_comment" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>Attenzione nel commento vi sono dei caratteri non permessi. Sono accettati solo i caratteri alfanumerici (A-Z a-z 0-9) e i seguenti simboli:  .,!&:_@$^;|<>\"\'=+*()èéòàùì</p></div>' +
            '<div ng-show="comment_data.show_warn_nickname_comment" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>Attenzione nel nickname e nel commento vi sono dei caratteri non permessi. Sono accettati solo i caratteri alfanumerici (A-Z a-z 0-9) e i seguenti simboli:  .,!&:_@$^;|<>\"\'=+*()èéòàùì</p></div>' +


          '</ion-content>' +




          '<div class="row" style="position:absolute; bottom:5px; padding:0px; left:5px; right:5px; width:auto;">' +

            '<div class="col" style="padding: 0px 2.5px 0px 0px;">' +
              '<button ng-click="inviaCommento()" class="button button-full button-balanced" style="margin:0px;">INVIA</button>' +
            '</div>' +

            '<div class="col" style="padding: 0px 0px 0px 2.5px;">' +
                '<button ng-click="annullaCommento()" class="button button-full button-stable" style="margin:0px;">ANNULLA</button>' +
            '</div>' +

          '</div>' +


      '</ion-modal-view>',

      {
          scope: $rootScope,
          focusFirstInput: true,
          animation :'none',
          hardwareBackButtonClose: false
      }

  );

  $rootScope.popupScreen.show();


};


})
