angular.module('LiveEngine.PoiDetail.Controller', [])

.filter('getUrl', function() {

   return function(url) {
       	return url ? url : './img/imgLoader.gif';
   }

})

.controller('PoiDetailCtrl', function(GlobalVariables, SupportServices,
$scope, $state, $ionicModal, $http, LazyLoader, HelperService, $ionicPopup,
LoggerService, FileSystemService, WikiService_poi, $ionicLoading, $timeout,
SocketService, GeoLocationService, LanguageService) {

  var self = this;

  $scope.data = {
      listaPercorsi: [],
      toDownload: [],
      timer: null,
      labels: {
        title: LanguageService.getLabel('SCEGLI_PERCORSO')
      },
      cssBlink: "blink_me",
      position: null,
      idPercorso: -1

  };

  $scope.$on("$ionicView.beforeEnter", function(event, data) {
      $ionicLoading.hide();
  });


  $scope.goBackToProgetto = function() {
      $ionicLoading.show({
          template: LanguageService.getLabel('ATTENDERE')
      });

      SupportServices.getListaCopertine({

          onComplete: function(err, jsonData) {

              $ionicLoading.hide();

              if(err) {

                  HelperService.showUnrecoverableError(err, false);

              } else {

                  GlobalVariables.application.listaProgetti = jsonData.progetti;
                  SocketService.closeSocket();
                  $state.go('home');

              }

          }

      });
  }


  $scope.loadPercorsi = function() {

      $ionicLoading.show({
          template: 'Attendere...'
      });

      SupportServices.getPercorsi({
          onComplete: function(err, json) {

              if(err) {

                  $ionicLoading.hide();

                  var alertPopup = $ionicPopup.alert({
                     title: LanguageService.getLabel('ATTENZIONE'),
                     template: LanguageService.getLabel('PROBLEMI_RETE')
                   });

                  return;
              }

              for(var i=0; i<json.percorsi.length; i++) {

                  var percorso = json.percorsi[i];

                  percorso.logoPercorso = GlobalVariables.baseUrl + "/" + percorso.logoPercorso;
                  if (percorso.nome=="Tutti") percorso.nome=LanguageService.getLabel('VICINO_A_ME'); 
                  $scope.data.listaPercorsi.push(percorso);

              }

              $ionicLoading.hide();

          }

      });

      /*
      setInterval(function() {
        var position = GeoLocationService.getCurrentPosition();
        console.log(position.coords.latitude + " - " + position.coords.longitude + " - " + position.coords.altitude + " - " + position.coords.accuracy);
      }
    , 1000);
    */

  };



  $scope.clickOnPercorso = function(idPercorso) {

 

      console.log("click on percorso");

      var position = GeoLocationService.getCurrentPosition();

      //position=""; //per provare nel browser

      if (position==null) //posizione non disponibile
      {
        
        HelperService.showGenericMessage("GPS non disponibile, attendere...", true); 
        //$scope.data.cssBlink="blink_me";
        //clearInterval($scope.data.timer);
        //$scope.startTimerGps();
        return;
      }

      /*
      $ionicLoading.show({
          template: 'Attendere...'
      });
      */
      
      $scope.data.position=position;
      $scope.data.idPercorso=idPercorso;


      $ionicLoading.show({
          template: '<div style="padding:10px 5px;">'+LanguageService.getLabel('AVVIO_REALTA_AUMENTATA')+'</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
          duration: 10000
      }); 

      SupportServices.getPoi({
          idPercorso: idPercorso,
          onComplete: function(err, json) {

              if(err) {

                  $ionicLoading.hide();

                  var alertPopup = $ionicPopup.alert({
                     title: LanguageService.getLabel('ATTENZIONE'),
                     template: LanguageService.getLabel('PROBLEMI_RETE')
                   });

                  return;
              }

              console.log("POI per HS");
              console.log(json);

              //devo scaricare un certo numero di files, prima di partire
              $scope.data.toDownload = new Array();
              var basePath=GlobalVariables.baseUrl + "/";
              var poi=json.poi;
              for (var i=0;i<poi.length;i++)
              {
                //ha un hotspot custom nuovo stile?
                var hsc_tipo=poi[i].hsc_tipo;
                if (typeof hsc_tipo != 'undefined')
                {
                  console.log("Trovato hotspot nuovo stile da scaricare: " + poi[i].hsc_path);
                  $scope.data.toDownload.push(basePath + poi[i].hsc_path);
                }

              }


              console.log($scope.data.toDownload);

              if ($scope.data.toDownload.length>0)
              {
                $scope.downloadFilesAndStart(0); //elemento iniziale
              }
              else
              {
                $scope.startRAPoi();
              }


          }

      });



      
/*


      setTimeout(function() {
          var localPath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;
          var localDynamicARPath = FileSystemService.getLocalPath() + "dinamic_classes" + GlobalVariables.directory_trail;
          //var idPercorso = idPercorso;
          var baseUrl = GlobalVariables.baseUrl;
          var parameter = { "path": "www/clientRecognition/indexPercorso.html", "requiredFeatures": ["geo"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "localDynamicARPath": localDynamicARPath, "idPercorso": idPercorso, "idCliente": GlobalVariables.idCliente, "baseUrl": baseUrl, "deviceId": GlobalVariables.deviceUUID, "position": position};
          //var parameter = { "path": "www/clientRecognition/indexPercorsoIndoor.html", "requiredFeatures": ["geo"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "localDynamicARPath": localDynamicARPath, "idPercorso": idPercorso, "idCliente": GlobalVariables.idCliente, "baseUrl": baseUrl, "deviceId": GlobalVariables.deviceUUID, "position": position};

          console.log(JSON.stringify(parameter));
          
          WikiService_poi.loadARchitectWorld(parameter);
        }
      , 1000);

*/


  };



  $scope.downloadFilesAndStart = function(index) {

      console.log("sono in downloadFilesAndStart, con index: " + index);

      path = $scope.data.toDownload[index];
      var pathSplit= path.split("/");
      //il nome � l'ultimo elemento
      var name=pathSplit[pathSplit.length-1];
      //e la dir il penultimo
      dir=pathSplit[pathSplit.length-2];

      var uri = encodeURI(path);

      var localpath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;

      //di base vedo se il file è già presente, per non doverlo scaricare
      console.log("Controllo se il file " + name + " esiste già, altrimenti provo a scaricarlo");
      FileSystemService.fileExists({
          directory: SupportServices.BUFFER_DIRECTORY + '/' + dir,
          fileName: name,
          onComplete: function(err, isAvailable) {

              var esiste=false;
              if(err)
              {
                  //errore, ma non faccio niente
                  console.log("Errore in fileExist, continuo e lo scaricherò");
              }
              else
              {
                  if(!isAvailable) {
                      //non esiste
                      console.log("Non Esiste, continuo e lo scaricherò");
                  } else {
                      //esiste, vado avanti
                      console.log("Esiste, vado avanti");
                      esiste=true;
                  }
              }
              if (esiste)
              {
                $scope.downloadNextFile(index);
              }
              else
              {
                //è presente la connessione?
                console.log("Check connessione: " + HelperService.isNetworkAvailable());
                if (HelperService.isNetworkAvailable()==false)
                {
                  console.log("Nessuna connessione. Ho già verificato che questo file non è presente...");
                  HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);
                }
                else
                {
                  console.log("Provo a scaricare " + uri);

                  FileSystemService.downloadFile({
                      url: uri,
                      directory: SupportServices.BUFFER_DIRECTORY + '/' + dir,
                      fileName: name,
                      onComplete: function(err) {

                          if(err) {
                            console.log("download error: " + err);
                            console.log("Ho già verificato che questo file non è presente...");
                            //il file non trovato, su ios, mi genera comunque un file con lo stesso nome ma con un contenuto non coerente (è la risposta del server che dice che il file non esiste)
                            //devo perciò esplicitamente cancellare questo file...

                            FileSystemService.fileDelete({
                                directory: SupportServices.BUFFER_DIRECTORY + '/' + dir,
                                fileName: name,
                                onComplete: function(err) {

                                    if(err) {
                                      console.log("filedelete error: " + err);
                                    } else {
                                      console.log("filedelete completato");
                                    }
                                    HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);
                                }
                            });

                          } else {
                            console.log("download completato");
                            //console.log("index: " + index);
                            //console.log("toDownload: " + toDownload);
                            $scope.downloadNextFile(index);

                          }
                      }
                  });
                }                
              }

          }
      });      





  }

  $scope.downloadNextFile = function(index) {

      index++;
      if (index<$scope.data.toDownload.length)
      {
        //console.log("index: " + index);
        $scope.downloadFilesAndStart(index);
      }
      else
      {

        console.log("Finito");

        $scope.startRAPoi();

      }

  }

  $scope.startRAPoi = function(index) {

    var localPath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;
    var localDynamicARPath = FileSystemService.getLocalPath() + "dinamic_classes" + GlobalVariables.directory_trail;
    //var idPercorso = idPercorso;
    var baseUrl = GlobalVariables.baseUrl;
    //var parameter = { "path": "www/clientRecognition/index.html", "requiredFeatures": ["2d_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "idPrj": idPrj, "isApiRest": false, "baseUrl": baseUrl, "deviceId": GlobalVariables.deviceUUID};
    var parameter = { "path": "www/clientRecognition/indexPercorso.html", "requiredFeatures": ["geo"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "localDynamicARPath": localDynamicARPath, "idPercorso": $scope.data.idPercorso, "idCliente": GlobalVariables.idCliente, "baseUrl": baseUrl, "deviceId": GlobalVariables.deviceUUID, "position": $scope.data.position};

    console.log(JSON.stringify(parameter));
    
    WikiService_poi.loadARchitectWorld(parameter);

  }


  $scope.$on("$ionicView.enter", function(event, data) {

      $scope.loadPercorsi();

      /*
        tweak per ios. quando esco dalla realtà aumentata di wikitude con avvenuta
        rotazione dello schermo l'ambientre ionic non percepisce il ricalcolo della larghezza
      */
      try {
        if(ionic.Platform.isIOS()) {
          StatusBar.show();
          StatusBar.hide();
        }  
      } catch(e) {}
      

  });


  $scope.$on("$ionicView.afterEnter", function(event, data) {

      $scope.startTimerGps();
  });



  $scope.$on("$ionicView.beforeLeave", function(event, data) {
      clearInterval($scope.data.timer);
  });

  $scope.startTimerGps = function() {
      
      $scope.data.timer = setInterval(function() {
        var position = GeoLocationService.getCurrentPosition();
        console.log(position);
        if (position!=null) //posizione rilevata
        {
          $scope.data.cssBlink="";
          clearInterval($scope.data.timer);
        }
        else
        {
          
        }
      }
    , 1000);

  }

});
