angular.module('LiveEngine.NavigazioneProgetti.Controller', [])

.filter('getUrl', function() {

   return function(url) {
       	return url ? url : './img/imgLoader.gif';
   }

})

.controller('NavigazioneProgettiCtrl', function(GlobalVariables, SupportServices,
$scope, $rootScope, $state, $ionicModal, $http, LazyLoader, HelperService, $ionicPopup,
$ionicLoading, $timeout, LoggerService, FileSystemService, SocketService, LanguageService) {

  $scope.data = {
      start: 0,
      limit: 20,
      progetto: null,
      layout: null,
      listaProgetti: [],
      moreDataCanBeloaded: true,
      numeri: LanguageService.getLabel('NUMERI')
  };


  $scope.goBackToProgetto = function() {
      //$state.go('testata_detail');
  
      $ionicLoading.show({
          template: LanguageService.getLabel('ATTENDERE')
      });

      SupportServices.getListaCopertine({

          onComplete: function(err, jsonData) {

              $ionicLoading.hide();

              if(err) {

                  HelperService.showUnrecoverableError(err, false);

              } else {

                  GlobalVariables.application.listaProgetti = jsonData.progetti;
                  SocketService.closeSocket();
                  $state.go('home');

              }

          }

      });

  };


  $scope.moreDataCanBeLoaded = function() {
      return $scope.data.moreDataCanBeLoaded;
  };


  $scope.loadProgetti = function() {

      $ionicLoading.show({
          template: LanguageService.getLabel('ATTENDERE')
      });

      SupportServices.getNumeriTestata({
          idTestata: GlobalVariables.application.currentTestata.idTestata,
          anno: null,
          start: $scope.data.start,
          limit: $scope.data.limit,
          onComplete: function(err, json) {

              if(err) {

                  $ionicLoading.hide();

                  var alertPopup = $ionicPopup.alert({
                     title: LanguageService.getLabel('ATTENZIONE'),
                     template: LanguageService.getLabel('PROBLEMI_RETE')
                   });

                  return;
              }

              SupportServices.getUsersByPrjId({
                  onComplete: function(err, json_prj) {
                    
                    console.log(JSON.stringify(json_prj));

                    var arrayPrjId=[];
                    if(err) {
                                              
                    }
                    else
                    {
                      arrayPrjId=json_prj.prjId_list;
                    }

                    console.log("--- JSON PROGETTI COUNTER:");
                    console.log(JSON.stringify(arrayPrjId));

                    for(var i=0; i<json.progetti.length; i++) {

                        var progetto = json.progetti[i];

                        progetto.immagineProgetto = GlobalVariables.baseUrl + "/" + progetto.immagineProgetto;
                        //qui devo mettere anche quanti utenti ci stanno su quel progetto
                        //cerco quell'id nell'array dei progetti e utenti
                        progetto.numUtenti=-1;
                        if (arrayPrjId.length>0)
                        {
                          for (var j=0;j<arrayPrjId.length;j++)
                          {
                            if (arrayPrjId[j].name==progetto.idProgetto) progetto.numUtenti=arrayPrjId[j].totale;
                          }
                        }                        
                        //se è rimasto a -1, non ci stavano utenti... faccio un random per metterne qualcuno...
                        //if (progetto.numUtenti==-1) progetto.numUtenti=Math.floor((Math.random() * 5) + 0);
                        if (progetto.numUtenti==-1) progetto.numUtenti=0;
                        $scope.data.listaProgetti.push(progetto);

                    }

                    if($scope.data.start + $scope.data.limit >= json.totale) {
                        $scope.data.moreDataCanBeLoaded = false;
                    } else {
                        $scope.data.moreDataCanBeLoaded = true;
                        $scope.data.start += $scope.data.limit;
                    }


                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.infiniteScrollComplete');

                    LoggerService.triggerAction({
                        action: LoggerService.ACTION.DATA_LOAD,
                        state: LoggerService.CONTROLLER_STATES.LISTA_PROGETTI,
                        data: null
                    });


                    
                  }

              });


          }

      });

  };



  $scope.clickOnProgetto = function(idProgetto) {

      console.log("click on progetto");
      GlobalVariables.application.currentProgetto = {
        idProgetto: idProgetto
      };

      $ionicLoading.show({
          template: LanguageService.getLabel('ATTENDERE')
      });

      SupportServices.sceltaProgetto({
          onComplete: function(err, json) {

              $ionicLoading.hide();
              if(err) {

                  var alertPopup = $ionicPopup.alert({
                     title: LanguageService.getLabel('ATTENZIONE'),
                     template: LanguageService.getLabel('PROBLEMI_RETE')
                   });

                  return;
              }

              $state.go('progetto_detail');

          }

      });


  };

  $scope.$on("$ionicView.beforeEnter", function(event, data) {

      $rootScope.showHome=true;

      // ############################## //
      // evento 'uscita da una rivista' //
      // ############################## //
      if(GlobalVariables.application.currentProgetto) {
        SocketService.emit('message','has_leaved_progetto', {}, {
          uid: GlobalVariables.deviceUUID,
          id: GlobalVariables.application.currentProgetto.idProgetto
        });
      }      

      $timeout(function() {
        GlobalVariables.application.currentProgetto = null;
        $scope.data.testata = GlobalVariables.application.currentTestata;
        $scope.data.layout = GlobalVariables.application.currrentLayoutTestata;
      },100);


  });

  $scope.$on("$ionicView.enter", function(event, data) {

      $scope.loadProgetti();



      /*
        tweak per ios. quando esco dalla realtà aumentata di wikitude con avvenuta
        rotazione dello schermo l'ambientre ionic non percepisce il ricalcolo della larghezza
      */
      try {
        if(ionic.Platform.isIOS()) {
          StatusBar.show();
          StatusBar.hide();
        }  
      } catch(e) {}
      

  });

  $scope.$on("$ionicView.afterEnter", function(event, data) {
    LoggerService.triggerAction({
        action: LoggerService.ACTION.ENTER,
        state: LoggerService.CONTROLLER_STATES.LISTA_PROGETTI,
        data: null
    });
  });

  $scope.$on("$ionicView.beforeLeave", function(event, data) {
    LoggerService.triggerAction({
        action: LoggerService.ACTION.LEAVE,
        state: LoggerService.CONTROLLER_STATES.LISTA_PROGETTI,
        data: null
    });

  });



});
