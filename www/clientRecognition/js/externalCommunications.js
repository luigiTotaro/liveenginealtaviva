function requestUsersNumber()
{
  document.location = 'architectsdk://getUsersNumber';
}

//ritorno dalla funzione di sopra
function getUsersNumber(numero)
{
  $("#badgeUtenti p").html(numero);
  $("#badgeUtenti").show();
}

function openContent(idImmagine,nome)
{
    document.location = 'architectsdk://openContent_' + encodeURIComponent(JSON.stringify({
      id: idImmagine,
      titolo: nome
    }));
}

function requestConnectionStatus()
{
  document.location = 'architectsdk://checkConnessione';
}

function openExternalLink(url)
{
  document.location = 'architectsdk://linkEsterno_'+url;
}

function existSlideshow(id)
{
  document.location = 'architectsdk://existSlideshow_'+id;
}

function scaricaHtmlZip(jsonConfig)
{
  document.location = 'architectsdk://scaricaHtmlZip_'+jsonConfig;
}

function exitRA()
{
  accendiLed (false);
  document.location = 'architectsdk://index.html';
}

//ritorno dalla richiesta di scaricare una immagine
function downloadElementStatus(testo)
{
  $("#loader").hide();

  if (testo=="ok") testo=getLabel("CONTENUTO_SALVATO","it","Contenuto salvato");
  $("#messaggioGenerico p").html(testo);
  $("#messaggioGenerico").show();

  setTimeout(function() {
    $("#messaggioGenerico").hide();
  }, 4000);

  back();
}

function navigaVersoPosizione(lat, lon)
{
  document.location = 'architectsdk://naviga_' + encodeURIComponent(JSON.stringify({
    lat: lat,
    lon: lon
  }));
}

