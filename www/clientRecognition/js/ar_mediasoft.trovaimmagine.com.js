var World = {
	loaded: false,
	rotating: false,


	init: function initFn() {
		this.loaded=false;
		//AR.context.destroyAll();
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {
		/*
			First an AR.ClientTracker needs to be created in order to start the recognition engine. It is initialized with a URL specific to the target collection. Optional parameters are passed as object in the last argument. In this case a callback function for the onLoaded trigger is set. Once the tracker is fully loaded the function worldLoaded() is called.

			Important: If you replace the tracker file with your own, make sure to change the target name accordingly.
			Use a specific target name to respond only to a certain target or use a wildcard to respond to any or a certain group of targets.

			Adding multiple targets to a target collection is straightforward. Simply follow our Target Management Tool documentation. Each target in the target collection is identified by its target name. By using this target name, it is possible to create an AR.Trackable2DObject for every target in the target collection.
		*/



		//alert("assets/"+oggetto.tracker);

		var targetCollectionResource = new AR.TargetCollectionResource(contenutiPath + wtcName);
		this.tracker = new AR.ImageTracker(targetCollectionResource, {
			onTargetsLoaded: this.worldLoaded
		});

		/*
		this.tracker = new AR.ClientTracker(contenutiPath + wtcName, {
			onLoaded: this.worldLoaded
		});
*/
		
		//this.rotationAnimation = new AR.PropertyAnimation(this.modelCar, "rotate.roll", -25, 335, 10000);

		arrayOverlay = [];

		//var trackable = new AR.Trackable2DObject(this.tracker, "*", {
		var trackable = new AR.ImageTrackable(this.tracker, "*", {
			drawables: {
				cam: arrayOverlay
			},
			//onEnterFieldOfVision: function onEnterFieldOfVisionFn(a)
			onImageRecognized: function onEnterFieldOfVisionFn(a)
            {
				console.log("onImageRecognized: " + a);

				if (rilevazioneAttiva)
				{
					rilevazioneAttiva=false;
					console.log("stoppo il rilevamento");
					rilevato(a);
					//onImageLost(a);
					setTimeout(function() {
						rilevazioneAttiva=true;
						console.log("abilito il rilevamento");
					}, 2000);					
				}
				else
				{
					console.log("ma la rilevazione non è temporaneamente attiva....");
				}

				//trackable.snapToScreen.enabled = false;
			},
			/*
			snapToScreen: {
				enabledOnExitFieldOfVision: false,
				snapContainer: document.getElementById('snapContainer')
			},
			*/	
			//onExitFieldOfVision: function onExitFieldOfVisionFn(a)
			onImageLost: function onExitFieldOfVisionFn(a)
            {
				console.log("onImageLost : " + a);
				//var cssDivInstructions = " style='display: table-cell;vertical-align: middle; text-align: center; width: 50%; padding-right: 15px;'";
				//document.getElementById('loadingMessage').innerHTML ="<div" + cssDivInstructions + ">Looking for Target.....</div>";

				//trackable.snapToScreen.enabled = false;
			},
		});


	},


	toggleAnimateModel: function toggleAnimateModelFn()
	{
		if (!World.rotationAnimation.isRunning()) {
			if (!World.rotating) {
				// Starting an animation with .start(-1) will loop it indefinitely.
				World.rotationAnimation.start(-1);
				World.rotating = true;
			} else {
				// Resumes the rotation animation
				World.rotationAnimation.resume();
			}
		} else {
			// Pauses the rotation animation
			World.rotationAnimation.pause();
		}

		return false;
	},



	worldLoaded: function worldLoadedFn() {

		console.log("worldLoaded");

		//comunico il tempo di inizio
		var date=new Date();
		
		var oggetto = new Object();
		oggetto.type="startRA";
		oggetto.key = GAME_KEY;
		oggetto.msg_body={"time": date.getTime()};
		
		document.location = 'architectsdk://messageFromAr'+encodeURIComponent(JSON.stringify(oggetto));
		//document.location = 'architectsdk://startFind'+date.getTime();
		
		//startTimerGioco();

		
	}



};


var logEnabled= false;
var language= "it";
var arrayAlert = new Array();

if (logEnabled==true)
{
	AR.logger.activateDebugMode();
}

console.log("sono qui dentro");



function passData(localPath, wtc, game_key, labels, systemLanguage) {

	console.log("v 1.0 - passData: "+localPath + " - " + wtc + " - " + game_key + " - " + labels + " - " + systemLanguage);
	//console.log("v 1.0 - passData: "+localPath + " - " + wtc);

	contenutiPath = localPath + "/";
	wtcName=wtc;
	language=systemLanguage;

	GAME_KEY = game_key;

	//rimetto le label come stavano prima, se sono state bonificate
	console.log("Labels AR prima: " + labels);
	labels= labels.replace(/\*\*\*/g,'\'');
	console.log("Labels AR dopo: " + labels);


	var labelArray = labels.split("|");
	if (labelArray[0]!="") $("#bottoneNonRiesco").html(labelArray[0]);
	if (labelArray[1]!="") abandonText=labelArray[1];
	if (labelArray[2]!="") abandonConfirmText=labelArray[2];
	if (labelArray[3]!="") abandonCancelText=labelArray[3];


	//quanto è grande la finestra?
	console.log("window.height: "+$(window).height());   // returns height of browser viewport
	console.log("document.height: "+$(document).height()); // returns height of HTML document
	console.log("window.width: "+$(window).width());   // returns width of browser viewport
	console.log("document.width: "+$(document).width()); 

	if (logEnabled==true) AR.logger.info("v 1.0 - passData: "+localPath);

	resizeBasedOnResolution();

	//$("#messaggioGenerico").show();


	setTimeout(function() {
		$("#bottoneNonRiesco").show();
	}, 10000);

	//setTimeout(function() {
		//$("#messaggioGenerico").hide();
	//}, 5000);

	loadJson(localPath+"/configuration.json");
	//World.init();

	//creo un timer

}

var baseUrl = "http://livengine.mediasoftonline.com";
var contenutiPath="";
var itemRilevato="";
var	oggetto = new Array();
var	arrayOverlay = new Array();
var rotationAnimation;
var labelsObj;
var timerAttuale=0.0;
var timerHandle;
var rilevazioneAttiva=true; //la blocco per 2 secondi quando ho rilevato e poi la rimetto
var GAME_KEY = null;
var abandonText = "Sei sicuro di voler abbandonare e dare la vittoria all'avversario?";
var abandonConfirmText = "Si,<br>abbandona";
var abandonCancelText = "No,<br>continuo";

$( window ).resize(function() {
  resizeBasedOnResolution();
});



function resizeBasedOnResolution()
{
	var larghezza=$( window ).width();
	var altezza=$( window ).height();
	
	//alert(larghezza);
	//alert(altezza);

	console.log( "larghezza:" + larghezza );
	console.log( "altezza:" + altezza );

	if (larghezza>altezza)
	{
		//landscape
		var larghDiv="50%";
		var margDiv="23%";
		if (larghezza<600)
		{
			larghDiv="60%";
			margDiv="18%";
		}

		$("#bottoneNonRiesco").css("width",larghDiv).css("left",margDiv);
		$("#messaggioGenerico").css("width",larghDiv).css("margin-left",margDiv).css("margin-top",((altezza-(altezza/3))/2)+"px");
		//$("#messaggioIniziale p").css("line-height",altezza/3+"px");
		$("#messaggioGenericoImg").css("width","25%")
		$("#confirmGenerico").css("width",larghDiv).css("margin-left",margDiv).css("margin-top",((altezza-(altezza/3))/2)+"px");
		$("#alertFondo").css("height",(altezza*0.1)+"px");

	}
	else
	{
		$("#bottoneNonRiesco").css("width","80%").css("left","10%");
		$("#messaggioGenerico").css("width","80%").css("margin-left","8%").css("margin-top",((altezza-(altezza/4))/2)+"px");
		//$("#messaggioIniziale p").css("line-height",altezza/4+"px");
		$("#messaggioGenericoImg").css("width","33%")
		$("#confirmGenerico").css("width","80%").css("margin-left","8%").css("margin-top",((altezza-(altezza/4))/2)+"px");
		$("#alertFondo").css("height",(altezza*0.08)+"px");

	}




}


function loadJson(myUrl)
{

	oggetto = new Object();
	console.log("carico il file: " + myUrl);

	if (logEnabled==true) AR.logger.info("url: " + myUrl)

	$.ajax({
		url: myUrl,
		dataType: 'json'
		})
		.done(function( json )
		{
			console.log("file json caricato");
			labelsObj=json.labels;
			
			//traduco le labels
			//$("#bottoneNonRiesco").html(getLabel("BTN_NON_RILEVO_COPERTINA","it","Non riesco a rilevare la copertina"));

			World.init();


			
		})
		.fail(function( jqXHR, textStatus ) {
	  		if (logEnabled==true) AR.logger.info("Request failed: " + textStatus)
	  		console.log( "Request failed: " + textStatus );

		}); 
}


function rilevato(itemRilevato)
{
	if (logEnabled==true) AR.logger.info("rilevato: "+ itemRilevato);
	console.log("sono in rilevato, con nome: "+itemRilevato);
	//alert("Rilevato "+itemRilevato);

	var oggetto = new Object();
	oggetto.type="AR_Image_Tracked";
	oggetto.msg_body={"target": itemRilevato};
	oggetto.key = GAME_KEY;
	document.location = 'architectsdk://messageFromAr'+encodeURIComponent(JSON.stringify(oggetto));

	/*
	if (itemRilevato==myItemToFind)
	{
		var date=new Date();

		var oggetto = new Object();
		oggetto.type="findTarget";
		oggetto.msg_body={"time": date.getTime(),"target": itemRilevato};
		document.location = 'architectsdk://messageFromAr'+encodeURIComponent(JSON.stringify(oggetto));
		//document.location = 'architectsdk://endFind'+date.getTime();

		clearInterval(timerHandle);
		$("#messaggioGenericoTesto").html("Hai trovato l'immagine!!!");
		$("#messaggioGenerico").show();
		setTimeout(function() {
			$("#messaggioGenerico").hide();
			document.location = 'architectsdk://';
		}, 5000);
	}
	*/
}


function getLabel(codice,lingua,defaultLabel)
{
	/*
	console.log("getLabel - " + codice + " - " + lingua + " - " + defaultLabel);
	var ret = defaultLabel;
	//cerco il codice nell'oggetto
	for (var i=0;i<labelsObj.length;i++)
	{
		if (labelsObj[i].codice==codice)
		{
			switch(lingua) {
			    case "it":
			        if ((labelsObj[i].it==null) || (labelsObj[i].it=="")) ret=labelsObj[i].itDefault;
			        else ret=labelsObj[i].it;
			        break;
			    case "en":
			        if ((labelsObj[i].en==null) || (labelsObj[i].en=="")) ret=labelsObj[i].enDefault;
			        else ret=labelsObj[i].en;
			        break;
			}
		}

	}
*/
	console.log("getLabel - " + codice + " - " + defaultLabel);
	var ret = defaultLabel;
	//cerco il codice nell'oggetto
	for (var i=0;i<labelsObj.length;i++)
	{
		if (labelsObj[i].codice==codice)
		{
		  var token=labelsObj[i];
		  ret = token[language] ? token[language] : token[language + 'Default'];
		}
	}





	return ret;

}


function messageForAR(json)
{
	console.log("dentro RA, messageForAR");
	console.log(json);

	json = decodeURIComponent(json);
	

	console.log("decodificato: " + json);
	var oggetto=JSON.parse(json)
	console.log("Parsato: " + JSON.stringify(oggetto));

	switch(oggetto.type) {
	    case "showTimer":
	        console.log("showTimer");
	        startTimerGioco();
	        break;
	    case "closeAR":
			document.location = 'architectsdk://chiudi'
	        break;
	    case "showAlert":	    	
    	    var messaggio = oggetto.msg_body.messaggio.replace(/\|/g, "'");
			console.log("sostituzione eventuali apici: " + messaggio);
	    	mostraAlertFondo(messaggio,oggetto.msg_body.duration);
	        break;
	    case "showCountdown":	    	
	        console.log("showCountdown");
    	    var time = parseInt(oggetto.msg_body.time);
	    	startCountdownGioco(time);
	        break;
	    default:
	        console.log("comando non riconosciuto");
	}	

}


function abandon()
{
	$( "#confirmGenericoTesto" ).text(abandonText);
	$( "#confirmConfirm" ).html(abandonConfirmText);
	$( "#confirmAnnulla" ).html(abandonCancelText);
	$( "#confirmConfirm" ).click(function() {
		var oggetto = new Object();
		oggetto.type="AR_Forfait";
		oggetto.key = GAME_KEY;
		oggetto.msg_body={};
		document.location = 'architectsdk://messageFromAr'+encodeURIComponent(JSON.stringify(oggetto));
		//document.location = 'architectsdk://abandon'
	});
	$( "#confirmAnnulla" ).click(function() {
	  $( "#confirmGenerico" ).hide();
	});
	$( "#confirmGenerico" ).show();

}

function startTimerGioco()
{
	$("#timerGioco").html("0");
	$("#timerGioco").show();
	timerAttuale=0;
	timerHandle = setInterval(function(){
		timerAttuale+=1;
		$("#timerGioco").html(timerAttuale.toFixed(0));
	}, 1000);
}

function startCountdownGioco(time)
{
	$("#timerGioco").html(time);
	$("#timerGioco").show();
	timerAttuale=time;
	timerHandle = setInterval(function(){
		timerAttuale-=1;
		if (timerAttuale>=0)
		{
			$("#timerGioco").html(timerAttuale.toFixed(0));
		}
		else
		{
			clearInterval(timerHandle);
			var oggetto = new Object();
			oggetto.type="AR_Countdown_finish";
			oggetto.msg_body={};
			oggetto.key = GAME_KEY;
			document.location = 'architectsdk://messageFromAr'+encodeURIComponent(JSON.stringify(oggetto));
		}
	}, 1000);
}

function mostraAlertFondo(messaggio, durata)
{
	//creo un oggetto alert, che poi verrà inserito nell'array per essere gestito dopo
    var indiceAlert=arrayAlert.length;
    var html='<div id="alertFondo_'+indiceAlert+'" style="position:absolute;bottom:6px;left:3%;right:3%;text-align: center;display:none;border: 0px solid black;background-color: rgba(255, 255, 255,0.9);box-shadow: #000000 3px 3px 5px;padding:2%;" >';
    html+='    <p class="testoBig" style="margin: 0px;">'+messaggio+'</p>';
    html+='</div>';
	$("body").append($(html));
	arrayAlert.push("alertFondo_"+indiceAlert);

	//sposto in alto tutti escluso l'ultimo
	spostAlert("alertFondo_"+indiceAlert); //passando l'indice dell'arte appena messo

	//$("#messaggioAlertFondo").text(messaggio);
	//$("#alertFondo").show();
	$("#alertFondo_"+indiceAlert).fadeIn( );

	setTimeout(function(){
		eliminaAlert();
	}, durata);
}

function eliminaAlert()
{
	//elimino l'aert più vecchio
	console.log("elimino Alert");
	for (var i=0;i<arrayAlert.length;i++)
	{
		if (arrayAlert[i]!="")
		{
			//lo elimino
			$("#"+arrayAlert[i]).fadeOut(  );
			//$("#"+arrayAlert[i]).hide();
			arrayAlert[i]="";
			//esco, non devo eliminare altro
			break;
		}
	}
}

function spostAlert(alertBasso)
{
	//vedo quanto è alto l'alert basso
	var altezzaAlert=$("#"+alertBasso).height();
	console.log(altezzaAlert);
	for (var i=0;i<arrayAlert.length;i++)
	{
		if ((arrayAlert[i]!="") && (arrayAlert[i]!=alertBasso))
		{
			//lo sposto
			//dove stava?
			var bottomAlert=parseInt($("#"+arrayAlert[i]).css("bottom"));
			console.log("bottom: " + bottomAlert);
			$("#"+arrayAlert[i]).css("bottom",(altezzaAlert+bottomAlert+25)+"px");

		}
	}

}


$(document).ready(function()
{
	//startTimerGioco();

	return;
	$("body").css("background","red");
	//$("#messaggioGenerico").show();
	resizeBasedOnResolution();
	$("#bottoneNonRiesco").show();
	//("#alertFondo").show();
	startTimerGioco();
	mostraAlertFondo("Questo è il messaggio da mostrare, ho trovato 2 indizi",4000);
	setTimeout(function() {
		mostraAlertFondo("messaggio 2",4000);
	}, 3000);	
	setTimeout(function() {
		mostraAlertFondo("messaggio 3",4000);
	}, 4000);	
	setTimeout(function() {
		mostraAlertFondo("messaggio 4",4000);
	}, 5000);	

});
	
