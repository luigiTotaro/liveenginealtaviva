var World = {
	loaded: false,
	rotating: false,


	init: function initFn() {
		this.loaded=false;
		//AR.context.destroyAll();
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {

		var self = this;
		this.tcr = new AR.TargetCollectionResource(contenutiPath + oggetto.wtc);
		
		this.tracker = new AR.ImageTracker(this.tcr, {
			maximumNumberOfConcurrentlyTrackableTargets: numeroMultiTarget,
			extendedRangeRecognition: AR.CONST.IMAGE_RECOGNITION_RANGE_EXTENSION.OFF,
			onTargetsLoaded: this.worldLoaded
		});


		arrayOverlay = [];
		arrayImages2Load = [];

		//creo l'oggetto che contiene tutti gli hotspot per questa rilevazione
		var overlayObject = new Hotspot(oggetto,AR,this);

		overlayObject.create(null);
		arrayOverlay=overlayObject.getArrayOverlay();
		arrayImages2Load=overlayObject.getArrayImages2Load();

		//var trackable = createTrackable(this.tracker,arrayOverlay);
		var trackable = new AR.ImageTrackable(this.tracker, "*", {
			onImageRecognized: function (a)
	        {
				console.log("ho rilevato " + a);
				//ciclo su tutti gli overlay, e aggiungo solo quelli che gli appartengono
				for (var i=0;i<arrayOverlay.length;i++)
				{
					if (arrayOverlay[i].targetRef==a) this.addImageTargetCamDrawables(a, arrayOverlay[i]);
				}
				scegliOverlay(a);
				$("#messaggioIniziale").hide(); //per sicurezza
				console.log("Pagine riconosciute: " + JSON.stringify(pagineRiconosciute));
			},
			onImageLost: function onExitFieldOfVisionFn(a)
	        {
				console.log("ho perso la rilevazione di " + a);
				paginaPersa(a);
				//ciclo su tutti gli overlay, e rimuovo solo quelli che gli appartengono
				for (var i=0;i<arrayOverlay.length;i++)
				{
					if (arrayOverlay[i].targetRef==a)
					{
						this.removeImageTargetCamDrawables(a, arrayOverlay[i]);
						arrayOverlay[i].enabled=false;
					}
				}
				console.log("Pagine riconosciute: " + JSON.stringify(pagineRiconosciute));
				if (pagineRiconosciute.length==0) startAnimationBar(RA_GENERALE);
			},
		});

		$("#backButton").show();
		console.log("ImageTrackable Pronto");
		document.getElementById('loadingMessage').innerHTML = getLabel("INQUADRA_PAGINA","Inquadra una pagina.....");
		startAnimationBar(RA_GENERALE);

	},

	mostraContenuti: function (indiceImmagine, indiceElemento)
	{
        return function() {
			//console.log("sono in mostraContenuti, con indiceImmagine: "+indiceImmagine + " e indiceElemento: " + indiceElemento);
			//console.log("dati: " +JSON.stringify(oggetto.target[indiceImmagine].punti[indiceElemento]));
			showContenuto(indiceImmagine, indiceElemento);
        };
	},

	worldLoaded: function worldLoadedFn() {
		console.log("worldLoaded");
		document.getElementById('loadingMessage').innerHTML = getLabel("INQUADRA_PAGINA","Inquadra una pagina.....");
	}


};

var pagineRiconosciute=new Array(); //per il multitarget mi dice quando sono a 0, in modo da rimettere la barra
var timerHeader=null;
var indiceHeaderPaginaMostrata=0; //indice all'interno dell'array che mi dice cosa sta mostrando e cosa mostrerà

var baseUrl = "http://192.170.5.25:8888/liveEngine_webapp";
var contenutiPath="";
var dynClassesPath="";
var idProgetto="";
var isApiRest = false;
var contenutiUrl = "";

var	oggetto = new Array();
var	arrayOverlay = new Array();
var	arrayImages2Load = new Array();

var aspectRationPlayerYT;
var deviceId="";
var swiper=null;
var oggettoAttesaConnessione;
var fullscreenVideo=true;
var fullscreenSwiper=true;
var arrayImagesSwiper=null;

var rotationAnimation;

var dynamicPosClass;
var arrayDynamicClasses = new Array();
var actualClass=null;
var showCommentsDisclaimer;
var commentsNickname;
var stopBarAnim=false;
var contenutoScelto=false;
var numeroMultiTarget=2; //default;
var popupOnScreen=false;
var language="it";
/*
function createTrackable(tracker,arrayOverlay)
{
	console.log("----createTrackable - " + tracker + " - " + arrayOverlay);

	var trackable = new AR.ImageTrackable(tracker, "*", {
		//drawables: {
		//	cam: arrayOverlay
		//},
		onImageRecognized: function (a)
        {
			console.log("----ho rilevato " + a);
			//paginaRilevata=true;

			//ciclo su tutti gli overlay, e aggiungo solo quelli che gli appartengono
			for (var i=0;i<arrayOverlay.length;i++)
			{
				if (arrayOverlay[i].targetRef==a)
				{
					this.addImageTargetCamDrawables(a, arrayOverlay[i]);
					//arrayOverlay[i].enabled=true;
				}
			}

			scegliOverlay(a);

			$("#messaggioIniziale").hide(); //per sicurezza
			console.log("Pagine riconosciute: " + JSON.stringify(pagineRiconosciute));

			//trackable.snapToScreen.enabled = false;
		},
		onImageLost: function onExitFieldOfVisionFn(a)
        {
			console.log("---ho perso la rilevazione di " + a);
			//paginaRilevata=false;
			//pagineRiconosciute--;
			paginaPersa(a);

			//ciclo su tutti gli overlay, e rimuovo solo quelli che gli appartengono
			for (var i=0;i<arrayOverlay.length;i++)
			{
				if (arrayOverlay[i].targetRef==a)
				{
					//console.log("considero l'oggetto: " + JSON.stringify(arrayOverlay[i]));
					this.removeImageTargetCamDrawables(a, arrayOverlay[i]);
					arrayOverlay[i].enabled=false;
				}
			}

			console.log("Pagine riconosciute: " + JSON.stringify(pagineRiconosciute));

			if (pagineRiconosciute.length==0)
			{
				//document.getElementById('loadingMessage').innerHTML = getLabel("INQUADRA_PAGINA","Inquadra una pagina.....");
				//disabilitatutto();
				//itemRilevato="";
				//trackable.snapToScreen.enabled = false;
				startAnimationBar(RA_GENERALE);
			}
		},
	});

	return trackable;

}

*/


console.log("sono dentro la RA");

//entry point da wikiservice....
function passData(localPath, localDynamicARPath, idPrj, apiRest, baseUrl, localDeviceId, loc_showCommentsDisclaimer, loc_commentsNickname, multitarget, systemLanguage)
{
	console.log("v 1.0 - passData: "+localPath+" - "+idPrj+ " - " + apiRest + " - " + baseUrl + " - " + localDeviceId + " - " + loc_showCommentsDisclaimer + " - " + loc_commentsNickname + " - " + multitarget + " - " + systemLanguage);
	//alert("v 1.0 - passData: "+localPath+" - "+idPrj+ " - " + apiRest + " - " + baseUrl + " - " + deviceId);

	contenutiPath = localPath + "/";
	dynClassesPath = localDynamicARPath + "/";
	idProgetto = idPrj;
	isApiRest = apiRest
	contenutiUrl = baseUrl;
	deviceId=localDeviceId
	showCommentsDisclaimer=loc_showCommentsDisclaimer;
	commentsNickname=loc_commentsNickname
	numeroMultiTarget=parseInt(multitarget) || 2;
	language=systemLanguage;

	resizeBasedOnResolution(RA_GENERALE);

	$("#messaggioIniziale").show();
	$("#barraScansione").show();

	requestUsersNumber();
	setInterval(function() {
		requestUsersNumber();
	}, 10000);

	//AR.hardware.camera.zoom  = 1;
	//alert(JSON.stringify(AR.hardware.camera.features));

	loadJsonLabels(localPath+"/configuration.json");
}

$( window ).resize(function() {
	resizeBasedOnResolution(RA_GENERALE);
});


function loadJsonLabels(myUrl)
{
	oggetto = new Object();
	//console.log("carico il file: " + myUrl);
	$.ajax({
		url: myUrl,
		dataType: 'json'
		})
		.done(function( json )
		{
			console.log("file json caricato");
			labelsObj=json.labels;
			//traduco le labels
			//$("#labelChiudi").html(getLabel("BTN_RITORNA","Ritorna"));
			$("#loadingMessage").html(getLabel("ATTENDERE","Attendere..."));
			$("#messaggiDownload p").html(getLabel("ATTENDERE_SCARICAMENTO_CONTENUTO","Attendere, scaricamento contenuto in corso"));
			$("#testoInquadraCopertina").html(getLabel("INQUADRA_PAGINA","Inquadra una pagina..."));
			
			loadJson(contenutiPath+"dettaglio_progetto_"+idProgetto+".json");
		})
		.fail(function( jqXHR, textStatus ) {
	  		console.log( "Request failed: " + textStatus );
		});
}

function loadJson(myUrl)
{
	oggetto = new Object();
	//console.log("carico il file: " + myUrl);
	$.ajax({
		url: myUrl,
		dataType: 'json'
		})
		.done(function( json )
		{
			//console.log("file json caricato");
			//layout
			//console.log ("Definizione Layout: " + JSON.stringify(json));
			//$( "#footerMsg" ).css("background-color",json.layout.coloreGenerale);
			//$( "#footerMsg" ).css("color",json.layout.coloreTesto);
			
			var col1=convertHex(json.layout.coloreGenerale,80);
			var col2=convertHex(json.layout.coloreGenerale,40);
			var col3=convertHex(json.layout.coloreGenerale,0);
			var col4=convertHex(json.layout.coloreGenerale,60);

			$( "#header" ).css("background","linear-gradient(to bottom, "+col1+", "+col2+"");
			$( "#subHeader" ).css("background","linear-gradient(to bottom, "+col2+", "+col3+"");
			$( "#footer" ).css("background","linear-gradient(to top, "+col4+", "+col3+"");

			//$( "#header" ).css("background-color",json.layout.coloreGenerale);
			$( "#header" ).css("color",json.layout.coloreTesto);
			$( "#loadingMessage" ).css("color",json.layout.coloreTesto);
			if (json.layout.testataNome=="") $( "#footerMsg" ).html(json.layout.nome + " AR App v1.0");
			else $( "#footerMsg" ).html(json.layout.testataNome + " - " + json.layout.nome + " AR App v1.0");
			//$( "#footer" ).show();
			$( "#textContainer" ).css("border","2px solid " + json.layout.coloreGenerale);
			$( "#videoContainer" ).css("border","2px solid " + json.layout.coloreGenerale);
			$( "#barraScansione" ).css("background-color",json.layout.coloreGenerale).css("-webkit-box-shadow","0px 0px 30px 2px "+json.layout.coloreGenerale).css("-moz-box-shadow","0px 0px 30px 2px "+json.layout.coloreGenerale).css("box-shadow","0px 0px 30px 2px "+json.layout.coloreGenerale);

			oggetto.coloreGenerale=json.layout.coloreGenerale;
			oggetto.gradimento=json.gradimento;
			oggetto.testataSocket=json.layout.testataSocket;

			//resizeBasedOnResolution(RA_GENERALE);

			var indice=0;
			//wtc
			var wtcPath=json.progetti[indice].wtcPath;
			wtcPathSplit=wtcPath.split("/");
			oggetto.wtc=wtcPathSplit[wtcPathSplit.length-2]+"/"+wtcPathSplit[wtcPathSplit.length-1];
			oggetto.idProgetto=json.progetti[indice].prjId;

			oggetto.target=json.progetti[indice].target;

			//console.log("fatto - external json");
			console.log(JSON.stringify(oggetto));
			
			//creo il contentManager che mi gestirà i contenuti visualizzati sul device
			contentManager = new ContentManager(contenutiUrl,contenutiPath,oggetto.coloreGenerale,RA_GENERALE);

			//prima del world.init, mi instanzio tutte le classi dinamiche
			var classToInit = new Array();
			for (i=0;i<oggetto.target.length;i++)
			{
				for (t=0;t<oggetto.target[i].punti.length;t++)
				{
					if (oggetto.target[i].punti[t].tipo==1000) //dinamico ()
					{
						var config=JSON.parse(oggetto.target[i].punti[t].config);
						if (typeof config.contenuto != 'undefined')
						{
							for (var x=0;x<config.contenuto.length;x++)
							{
								if (classToInit.indexOf(config.contenuto[x].tipo) == -1) classToInit.push(config.contenuto[x].tipo);
							}
						}
						else //nuova modalità
						{
							console.log("config:" + JSON.stringify(config));
							//il config è un array
							for (var x=0;x<config.length;x++)
							{
								var configInterno=JSON.parse(config[x].config);
								if (typeof configInterno.tipologia != 'undefined')
								{
									if (classToInit.indexOf(configInterno.tipologia) == -1) classToInit.push(configInterno.tipologia);
								}
							}
						}
					}
					else //vedo comunque all'interno, perchè ci potrebbe essere un contenuto dinamico non in prima posizione
					{
						var config=JSON.parse(oggetto.target[i].punti[t].config);
						console.log("config:" + JSON.stringify(config));
						//il config è un array
						for (var x=0;x<config.length;x++)
						{
							var tipo=parseInt(config[x].tipo);
							if (tipo>=1000) //classe dinamica
							{
								var configInterno=JSON.parse(config[x].config);
								if (typeof configInterno.tipologia != 'undefined')
								{
									if (classToInit.indexOf(configInterno.tipologia) == -1) classToInit.push(configInterno.tipologia);
								}
							}
						}

					}
				}
			}
			/*
			if (classToInit.length>0) //ci sta almeno una classe dinamica
			{
				classToInit.push("DynamicPos");
			}
			*/
			console.log("classToInit: " + JSON.stringify(classToInit));

			if (classToInit.length>0)
			{
				doRequire(classToInit,0); //funzione ricorsiva per fare il require di tutte le classi, alla fine chiama la world.init
			}
			else
			{
				console.log("faccio partire Wiki....");
				World.init();
			}
		})
		.fail(function( jqXHR, textStatus ) {
	  		console.log( "Request failed: " + textStatus );
		});

}


function scegliOverlay(itemRilevato)
{
	console.log("sono in scegliOverlay, con nome: "+itemRilevato);

	//se ci sta qualcosa in esecuzione (video, ecc) non continuo
	if (($("#videoContainer").is(":visible")) || ($("#textContainer").is(":visible")) || ($("#imageContainer").is(":visible")) || ($("#swiperDiv").is(':visible')))
	{
		return;
	}

	//se ci sta il popup di contenuti multipli non continuo
	if (popupOnScreen)
	{	
		return;
	}

	stopBarAnim=true;
	stopBarAnimFn();

	//nome da far comparire in alto
	var nome="";
	var gradimento;
	var idImmagine;
	var scaricabile;
	var imagePath;
	var tipo;
	var autopartente = null; //se incontra un hotspot autopartente, memorizzerà qui target, punto e indice
 
	for (i=0;i<oggetto.target.length;i++)
	{
		if (oggetto.target[i].targetName==itemRilevato)
		{
			console.log(JSON.stringify(oggetto.target[i]));
			nome=oggetto.target[i].imageName;
			gradimento=oggetto.target[i].gradimento;
			voto=oggetto.target[i].voto;
			voti=oggetto.target[i].voti;
			idImmagine=oggetto.target[i].targetName;
			scaricabile=oggetto.target[i].scaricabile;
			imagePath=oggetto.target[i].imagePath;
			tipo=oggetto.target[i].tipo;
			// e comunico all'esterno che ha rilevato l'immagine
			openContent(idImmagine,nome);			
			
			//cerco eventuale autopartente
			for (t=0;t<oggetto.target[i].punti.length;t++)
			{
				var oggettoConfig=JSON.parse(oggetto.target[i].punti[t].config);
				for (z=0;z<oggettoConfig.length;z++)
				{
					console.log("considero l'indice "+z);
					if (oggettoConfig[z] != null)
					{					
						if (typeof oggettoConfig[z].autostart != 'undefined')
						{
							if (oggettoConfig[z].autostart==1)
							{
								autopartente = new Object();
								autopartente.immagine=i;
								autopartente.punto=t;						
								autopartente.indice=z;						
							}
						}
					}
				}
			}
		}
	}
	
	console.log("Autopartente: " + JSON.stringify(autopartente));

	//se ci sta solo una pagina riconosciuta mostro il nome, altrimenti niente
	pagineRiconosciute.push(nome);
	aggiornaHeaderMessage();

	var time=0;
	for (i=0;i<arrayOverlay.length;i++)
	{
		if (arrayOverlay[i].targetRef==itemRilevato)
		{
			console.log("trovato oggetto da abilitare: " + JSON.stringify(arrayOverlay[i]));
			abilitaOggetto(arrayOverlay[i],time);
			time = time+50;
		}
		else
		{
			//arrayOverlay[i].enabled=false; //ci pensa la procedura di onImageLost a disabilitare
		}
	}
	//check autopartenza
	if (autopartente!=null) showContenutoExecute(autopartente.immagine, autopartente.punto, autopartente.indice);
}

function paginaPersa(itemPerso)
{
	var nome="";
	for (i=0;i<oggetto.target.length;i++)
	{
		if (oggetto.target[i].targetName==itemPerso) nome=oggetto.target[i].imageName;
	}

	var index = pagineRiconosciute.indexOf(nome);
	if (index > -1) pagineRiconosciute.splice(index, 1);
	//se ci sta il popup dei contenuti multipli, non aggiorno l'header
	if (!popupOnScreen) aggiornaHeaderMessage();

}
 
function aggiornaHeaderMessage()
{
	if (timerHeader) clearInterval(timerHeader);
	if (pagineRiconosciute.length==0)
	{
		indiceHeaderPaginaMostrata=0;
		document.getElementById('loadingMessage').innerHTML = getLabel("INQUADRA_PAGINA","Inquadra una pagina.....");
	}
	else if (pagineRiconosciute.length==1)
	{
		indiceHeaderPaginaMostrata=0;
		document.getElementById('loadingMessage').innerHTML = pagineRiconosciute[0] ;
	}
	else
	{
		timerHeader = setInterval(function() {
			document.getElementById('loadingMessage').innerHTML = pagineRiconosciute[indiceHeaderPaginaMostrata];
			indiceHeaderPaginaMostrata++;
			if (indiceHeaderPaginaMostrata>=pagineRiconosciute.length) indiceHeaderPaginaMostrata=0; //per ricominciare
		}, 1000);
	}

}

function abilitaOggetto(obj,time)
{
	console.log("sono in abilitaOggetto, con time=" + time);
	setTimeout(function() {
		console.log(JSON.stringify(obj));

		obj.enabled=true
		//obj.scale=1;
		if (obj.customType==1)
		{
			if (typeof obj.rotationAnimation != 'undefined')
			{
				obj.rotationAnimation.start(-1);
			}
		}
	}, time);
}

function showContenuto(immagine, punto)
{
	contenutoScelto=true;

	console.log("sono in showContenuto, con immagine: "+immagine+ " e punto: " + punto);

	//nel config ci sarà il configExt, se non nullo, altrimenti il config vecchio in forma di configExt
	//è da attivare la finestra con i contenuti multipli, ma solo se ci sono più di un contenuto
	var oggettoConfigExt=JSON.parse(oggetto.target[immagine].punti[punto].config);
	if (oggettoConfigExt.length>1)
	{
		//quando viene mostrato il popup, il riconoscimento della RA deve essere disabilitato
		popupOnScreen=true;
		creaPopupSceltaContenuto(RA_GENERALE, oggettoConfigExt, oggetto.target[immagine].punti[punto].label, immagine, punto);
	}
	else // solo un contenuto, inutile mostrare la lista
	{
		showContenutoExecute(immagine, punto, 0);		
	}
}

function showContenutoExecute(immagine, punto, indice)
{
	accendiLed (false);

	console.log("sono in showContenutoExecute, con immagine: "+immagine+ " punto: " + punto + " e indice: " + indice);
	if (popupOnScreen)
	{
		popupOnScreen=false;
		aggiornaHeaderMessage();
		$("#infoContainer").hide();
	}

	//se passo null come indice, vuol dire che considero il vecchio config
	//altrimenti considero il nuovo e mi rappresenta l'indice all'interno dell'array dei contenuti	
	var oggettoPunto;
	var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
	oggettoPunto=oggettoContenuti[indice];

	//per alcuni conteuti è necessaria la verifica della connessione
	//in quel caso, mi memorizzo i dati che verranno usati al ritorno della funzione
	console.log("tipo: " + oggettoPunto.tipo);
	switch (parseInt(oggettoPunto.tipo)) {
	    case 1: //video
	    case 2: //slideshow
	    case 5: //elemento scaricabile
	    case 7: //audio
	    case 1001: //elemento collaborativo - votazione, gradimento
	    case 1002: //elemento collaborativo - commento
	    case 1003: //elemento collaborativo - condivisione
			console.log("sono nel primo case");
			oggettoAttesaConnessione = new Object();
			oggettoAttesaConnessione.tipo = oggettoPunto.tipo;
			oggettoAttesaConnessione.immagine = immagine;
			oggettoAttesaConnessione.punto = punto;
			oggettoAttesaConnessione.indice = indice;
			$("#loader").show();
			requestConnectionStatus();
	        break;
	    case 1010: //calcolo matriciale - dinamico
			var oggettoDinamico;
			oggettoDinamico=JSON.parse(oggettoPunto.config);
			var tipologia=oggettoDinamico.tipologia;
			actualClass==null;
			console.log("tipologia da cercare: " + tipologia);
			for (var i=0;i<arrayDynamicClasses.length;i++)
			{
				console.log("classe "+i+" chiSono: " + arrayDynamicClasses[i].chiSono());
				if (arrayDynamicClasses[i].chiSono()==tipologia) actualClass=arrayDynamicClasses[i];
			}
			//ho una classe non nulla?
			if (actualClass==null) return;
			//disabilito temporaneamente la camera
			AR.hardware.camera.enabled = false;
			// e disabilito tutto
			disabilitatutto();
			// e faccio partire la classe dinamica
			actualClass.renderMainExt(immagine, punto, indice, deviceId);
	        break;
	    case 3: //testo
			console.log("sono nel secondo case");
			oggettoTesto=JSON.parse(oggettoPunto.config);
			contentManager.setTesto(oggettoTesto);
	        break; 
	    case 4: //html zip
			console.log("sono nel terzo case");
			//per questo tipo di contenuto devo prima scaricare il file, scompattarlo e dopo eseguirlo...
			$("#loader").show();
			scaricaHtmlZip(oggettoPunto.config);
	        break;
	    case 6: //link esterno
			console.log("sono nel quarto case");
			oggettoLink=JSON.parse(oggettoPunto.config);
			
			//ci sono dei parametri usciti fuori da qualche classe dinamica?
			var qs="";
			if ((typeof queryStringArray != 'undefined') && (queryStringArray.length>0))
			{
				qs="?i=0"; // serve solo per far mattere si seguito l'&
				for (var i=0;i<queryStringArray.length;i++)
				{
					qs+="&"+queryStringArray[i];
				}
			}
			
			console.log("URL completo: " + oggettoLink.url + qs);
			openExternalLink(oggettoLink.url + qs);
			//e dico che non ha scelto nessun contenuto, in modo che ritornando ricompaia la barra di scansione
			back();
			/*
			contenutoScelto=false;
			// e se ci stava un popup di scelta, lo tolgo
			if (popupOnScreen)
			{
				popupOnScreen=false;
				aggiornaHeaderMessage();
			}
			*/
	        break; 
	    default: 
			console.log("sono nel case default");
	        //default, mai eseguito;
	}	

}

function checkConnessione(status)
{
	//alert("status: " + status);
	$("#loader").hide();
	console.log("tornato da check connessione, con status: " + status);
	console.log("oggettoAttesaConnessione.tipo: " + oggettoAttesaConnessione.tipo);


	if (status==true) //connessione presente
	{
		if (oggettoAttesaConnessione.tipo==1) //video
		{
			var audio = document.getElementById('audio');
			if (audio.currentSrc != "") pausaAudio(); //in caso un audio è in esecuzione

			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var oggettoVideo;

			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			oggettoVideo=JSON.parse(oggettoPunto.config);
			contentManager.setVideo(oggettoVideo);
		}
		else if (oggettoAttesaConnessione.tipo==2) //slideshow
		{
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var arrayOggettoImmagine;

			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			arrayOggettoImmagine=JSON.parse(oggettoPunto.config);
			contentManager.setSlideshow(arrayOggettoImmagine, true, oggetto.target[immagine].punti[punto].id); //connessione presente
		}
		else if (oggettoAttesaConnessione.tipo==5) //elemento scaricabile
		{
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var oggettoScaricabile;

			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			oggettoScaricabile=JSON.parse(oggettoPunto.config);

			contentManager.setElementoScaricabile(oggettoScaricabile);
		}
		if (oggettoAttesaConnessione.tipo==7) //audio
		{
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var oggettoAudio;

			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			oggettoAudio=JSON.parse(oggettoPunto.config);
			contentManager.setAudio(oggettoAudio);
		}
		else if ((oggettoAttesaConnessione.tipo==1001) || (oggettoAttesaConnessione.tipo==1002) || (oggettoAttesaConnessione.tipo==1003))//votazione, commenta, share
		{
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var oggettoDinamico;
			
			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			oggettoDinamico=JSON.parse(oggettoPunto.config);
			var tipologia=oggettoDinamico.tipologia;
			actualClass==null;
			console.log("tipologia da cercare: " + tipologia);
			for (var i=0;i<arrayDynamicClasses.length;i++)
			{
				console.log("classe "+i+" chiSono: " + arrayDynamicClasses[i].chiSono());
				if (arrayDynamicClasses[i].chiSono()==tipologia) actualClass=arrayDynamicClasses[i];
			}
			//ho una classe non nulla?
			if (actualClass==null) return;
			//disabilito temporaneamente la camera
			AR.hardware.camera.enabled = false;
			// e disabilito tutto
			disabilitatutto();
			// e faccio partire la classe dinamica
			actualClass.renderMainExt(immagine, punto, indice, deviceId);
		}
		else if (oggettoAttesaConnessione.tipo==1000)
		{
			//ci sta la connessione, posso mostrare il contenuto dinamico
			
			var time=0;
			//abilito a seconda dell'oggetto
			for (i=0;i<arrayOverlay.length;i++)
			{
				if (arrayOverlay[i].targetRef==oggettoAttesaConnessione.itemRilevato)
				{
					if (arrayOverlay[i].customType==1000)
					{	
						console.log("abilito oggetto: " + JSON.stringify(arrayOverlay[i]));
						abilitaOggetto(arrayOverlay[i],time);
						time = time+50;
					}		
				}
			}
		}
		console.log("cancello oggettoAttesaConnessione");
		oggettoAttesaConnessione = null;
		delete oggettoAttesaConnessione;
	}
	else
	{
		//alert("Nessuna connessione disponibile");
		//ma per lo slideshow potrei aver cacheato i files
		if (oggettoAttesaConnessione.tipo==2)
		{
			//ora devo verificare se qualcosa era stato salvato in precedenza
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			console.log("vado a existSlideshow, con oggettoAttesaConnessione: " + JSON.stringify(oggettoAttesaConnessione));
			existSlideshow(oggetto.target[immagine].punti[punto].id);
			//e aspetto una risposta, non cancellando oggettoAttesaConnessione che mi potrà servire...
		}
		else if (oggettoAttesaConnessione.tipo==1000) //contenuto dinamico.. non faccio niente
		{
			console.log("non posso abilitare gli oggetti dinamici");
			console.log("cancello oggettoAttesaConnessione");
			oggettoAttesaConnessione = null;
			delete oggettoAttesaConnessione;		
		}
		else
		{
			$("#messaggioGenerico p").html(getLabel("NO_CONNESSIONE","Nessuna connessione disponibile"));
			$("#messaggioGenerico").show();

			contenutoScelto=false;

			setTimeout(function() {
				$("#messaggioGenerico").hide();
			}, 4000);

			console.log("cancello oggettoAttesaConnessione");
			oggettoAttesaConnessione = null;
			delete oggettoAttesaConnessione;			
		}
	}

}

function checkConnessioneSlideshow(status) //richiamata quando non ci stava connessione e voglio vedere se lo lideshow esiste in locale, salvato in precedenza
{
	console.log("tornato da existSlideshow, con oggettoAttesaConnessione: " + JSON.stringify(oggettoAttesaConnessione));
	if (status==true) //qualche immagine dello slideshow è presente
	{
		var immagine=oggettoAttesaConnessione.immagine;
		var punto=oggettoAttesaConnessione.punto;

		var arrayOggettoImmagine=JSON.parse(oggetto.target[immagine].punti[punto].config)
		contentManager.setSlideshow(arrayOggettoImmagine, false, oggetto.target[immagine].punti[punto].id); //connessione non presente
	}
	else
	{
		//alzo definitivamente bandiera bianca
		$("#messaggioGenerico p").html(getLabel("NO_CONNESSIONE","Nessuna connessione disponibile"));
		$("#messaggioGenerico").show();

		contenutoScelto=false;

		setTimeout(function() {
			$("#messaggioGenerico").hide();
		}, 4000);	
	}
	console.log("cancello oggettoAttesaConnessione");
	oggettoAttesaConnessione = null;
	delete oggettoAttesaConnessione;	
}

function back() //quando si chiudono elementi come video o slideshow, o pupup contenuti multipli
{
	if (popupOnScreen) //ci stava il popup
	{
		popupOnScreen=false;
		aggiornaHeaderMessage();
	}

	backUtils(RA_GENERALE);
}



$(document).ready(function()
{


	setTimeout(function() {
		resizeBasedOnResolution(RA_GENERALE);
	}, 100);	

	setTimeout(function() {
		resizeBasedOnResolution(RA_GENERALE);
	}, 1000);
	/*
	setTimeout(function() {
		var color=$("body").css("background");
		var color2=$("body").css("background-color");

		alert(color + " - " + color2);
	}, 3000);	
*/

	return;
	$("#backButton").show();
	
	$("#loadingMessage").html("Attendere...");
	$("#messaggiDownload p").html("Attendere, scaricamento contenuto in corso");
	$("#testoInquadraCopertina").html("Inquadra una pagina...");
		
	//return;	
	var col1=convertHex("#FF0000",80);
	var col2=convertHex("#FF0000",40);
	var col3=convertHex("#FF0000",0);
	var col4=convertHex("#FF0000",60);

	$( "#header" ).css("background","linear-gradient(to bottom, "+col1+", "+col2+"");
	$( "#subHeader" ).css("background","linear-gradient(to bottom, "+col2+", "+col3+"");
	$( "#footer" ).css("background","linear-gradient(to top, "+col4+", "+col3+"");

	$("#badgeUtenti p").html("25");
	$("#badgeUtenti").show();

	resizeBasedOnResolution(RA_GENERALE);

	
	contentManager = new ContentManager("","","#ff0000");
	
	//var url=decodeURIComponent("http://livengine.mediasoftonline.com/test/Mazinga.mp3");
	var url="http://livengine.mediasoftonline.com/test/Mazinga.mp3";
	//var url=decodeURIComponent("https://www.yt2mp3s.me/@download/256-5ac4aa1fcafb1-6496000-203/mp3/D05mceE9CHk/Mazinga%2BZ%2Bsigla%2Bil%2BGrande%2BMazinger.mp3");

	//contentManager.creaAudio(url,"LINK");

	setTimeout(function() {
	    var audio = document.getElementById('audio');
	    console.log(audio.currentSrc);


		if (audio.currentSrc != "") pausaAudio(); //in caso un audio è in esecuzione
		contentManager.creaVideo("H4f2A4LlgX8","YT",1.777778);

	}, 5000);


	return;
	contentManager = new ContentManager("","","#ff0000");
	
	//var url=decodeURIComponent("http://livengine.mediasoftonline.com/test/Mazinga.mp3");
	var url="http://livengine.mediasoftonline.com/test/Mazinga.mp3";
	//var url=decodeURIComponent("https://www.yt2mp3s.me/@download/256-5ac4aa1fcafb1-6496000-203/mp3/D05mceE9CHk/Mazinga%2BZ%2Bsigla%2Bil%2BGrande%2BMazinger.mp3");

	contentManager.creaAudio(url,"LINK");


	setTimeout(function() {
		
		var url=decodeURIComponent("https://www.yt2mp3s.me/@download/256-5ac4aa1fcafb1-6496000-203/mp3/D05mceE9CHk/Mazinga%2BZ%2Bsigla%2Bil%2BGrande%2BMazinger.mp3");
		contentManager.creaAudio(url,"LINK");

	}, 500000);


	//solo funzioni per testare nel browser

	//var oggettoConfigExt=JSON.parse('[{"tipo":1,"label":"un bel video","config":"{ \'path\':\'http%3A%2F%2Fvideoupload.mondadori.it%2Fserver%2Fphp%2Ffiles%2Fftp%2FCHI%2FSICILY.mp4\',\'nome\':\'Video\',\'type\':\'LINK\'}"},{"tipo":2,"label":"uno Slideshow","config":"[{\'ordine\':\'0\',\'path\':\'contenuti%2F710_3_1506328303_internazionale_h_00_ok.jpg\',\'nome\':\'internazionale_h_00_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'1\',\'path\':\'contenuti%2F710_3_1506328322_internazionale_h_07_ok.jpg\',\'nome\':\'internazionale_h_07_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'2\',\'path\':\'contenuti%2F710_3_1506328317_internazionale_h_08_ok.jpg\',\'nome\':\'internazionale_h_08_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'3\',\'path\':\'contenuti%2F710_3_1506328307_internazionale_h_04_ok.jpg\',\'nome\':\'internazionale_h_04_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'4\',\'path\':\'contenuti%2F710_3_1506328305_internazionale_h_09_ok.jpg\',\'nome\':\'internazionale_h_09_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'5\',\'path\':\'contenuti%2F710_3_1506328304_internazionale_h_01_ok.jpg\',\'nome\':\'internazionale_h_01_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'6\',\'path\':\'contenuti%2F710_3_1506328308_internazionale_h_02_ok.jpg\',\'nome\':\'internazionale_h_02_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'7\',\'path\':\'contenuti%2F710_3_1506328309_internazionale_h_03_ok.jpg\',\'nome\':\'internazionale_h_03_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'8\',\'path\':\'contenuti%2F710_3_1506328312_internazionale_h_05_ok.jpg\',\'nome\':\'internazionale_h_05_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'9\',\'path\':\'contenuti%2F710_3_1506328314_internazionale_h_06_ok.jpg\',\'nome\':\'internazionale_h_06_ok.jpg\',\'type\':\'LOCAL\'}]"},{"tipo":3,"label":"Un testo","config":"{\'text\':\'<b>STRUTTURA</b><br>Il Montaperti Hotel è situato a circa 15 km da Siena ed è il perfetto connubio tra il design moderno e il paesaggio rurale delle colline senesi. Immersa tra i profumi e i colori dell’incontaminata natura toscana, è la struttura ideale per trascorrere un soggiorno in relax e per godersi la natura.<br><br><b>LA QUOTA COMPRENDE</b><br>Mezza pensione con colazione a buffet e cena con servizio al tavolo con menù di 2 portate (un primo e un dolce), utilizzo della piscina riscaldata con idromassaggio (coperta nei mesi invernali), utilizzo del fitness center e del centro benessere che comprende sauna, bagno turco, docce emozionali, doccia a diluvio con secchiello, cascata di ghiaccio (ingresso al centro benessere non consentito il sabato), sconto del 10% per le Terme di Rapolano, connessione Wi-Fi, parcheggio secondo disponibilità, culla su richiesta alla prenotazione secondo disponibilità.<br><br><b>LA QUOTA NON COMPRENDE</b><br>Bevande ai pasti, kit benessere con accappatoio e ciabattine (a pagamento in loco), eventuale tassa di soggiorno applicata in loco, extra in genere e tutto quanto non specificato nel paragrafo \'La quota comprende\'.<br><br><b>RIDUZIONI</b><br>Riduzioni 3° letto (valide con almeno 2 persone paganti quota intera):<br>• 0 -6 anni non compiuti GRATIS<br>• 6 - 13 anni non compiuti 50%<br>• da 13 anni in poi 25%<br><br><b>ORARIO CHECK-IN/OUT</b><br>Check-in dalle ore 14:00 Check-out entro ore 11:00<br><br><b>CAMERE</b><br>Le camere Comfort (circa 18-20 mq), situate al piano terra con affaccio sulla corte, sono dotate di servizi privati con vasca o doccia, asciugacapelli, aria condizionata, TV Sat, telefono, cassaforte, frigobar e connessione Wi-Fi.  SERVIZI A disposizione degli ospiti reception 24h, ascensore, ristorante, bar, internet point, connessione Wi-Fi (gratuita), A disposizione degli ospiti reception 24h, ascensore, ristorante, bar, internet point, connessione Wi-Fi (gratuita), piscina esterna attrezzata con lettini secondo disponibilità, deposito bagagli, servizio in camera, servizio lavanderia, area fumatori, quotidiani, parcheggio privato secondo disponibilità (gratuito), culla su richiesta al call center al momento della prenotazione (gratuita). Inoltre l\'Hotel mette a disposizione un moderno centro benessere con piscina coperta riscaldata con idromassaggio, sauna, bagno turco, doccia a secchio e cascata di ghiaccio, fitness corner e terrazza solarium (l\'accesso al centro benessere è consentito dalle 10:00 alle 19:00).<br><br><b>AMICI A 4 ZAMPE</b> Non ammessi.\'}"},{"tipo":5,"label":"Download","config":"{\'path\':\'contenuti/202_0.jpg?t=1491475851551\',\'type\':\'LOCAL\',\'descr\':\'Palinsesto 3 aprile\'}"},{"tipo":6,"label":"Sito Web","config":"{\'url\':\'http%3A%2F%2Fwww.lecceprima.it%2Fristoranti%2Fla-staffa-ristorante-bar-pizzeria-tavola-calda-1870736.html\'}"}]');
	//console.log(oggettoConfigExt);
	//creaPopupSceltaContenuto(RA_GENERALE, oggettoConfigExt, "Ciao Ciao", 100, 2);

/*
	contentManager = new ContentManager("","","#ff0000");
	var arrayImages=new Array();
	arrayImages.push("");
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638560_slide4.jpg";
	arrayImages.push(img);
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638562_slide3.jpg";
	arrayImages.push(img);
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638567_slide1.jpg";
	arrayImages.push(img);
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638568_slide2.jpg";
	arrayImages.push(img);
	fullscreenSwiper=true;
	contentManager.creaSlideshowNew(arrayImages,true);
*/

/*
	contentManager = new ContentManager("","","#ff0000");
	fullscreenVideo=true;
	contentManager.creaVideo("H4f2A4LlgX8","YT",1.777778);
	//contentManager.creaVideo("http://livengine.mediasoftonline.com/test/chi_ventura.mp4","LINK",0);
*/
	//resizeBasedOnResolution(RA_GENERALE);
	//$("#backButton").show();
	//getUsersNumber(4);
	/*
	$("body").css("background-color","#000000");
	resizeBasedOnResolution(RA_GENERALE);
	var arrayImages=new Array();
	arrayImages.push("");
	var img = "http://www.cliccascienze.it/files/isola.jpg";
	arrayImages.push(img);
	img = "https://www.greenme.it/immagini/viaggiare/eco-turismo/isole_cuore/isole_a_forma_di_cuore.jpg";
	arrayImages.push(img);
	
	creaSlideshowNewTEST(arrayImages);
*/
	return;

});
