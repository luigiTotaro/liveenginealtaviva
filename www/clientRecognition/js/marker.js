function Marker(poiData) {

    /*
        For creating the marker a new object AR.GeoObject will be created at the specified geolocation. An AR.GeoObject connects one or more AR.GeoLocations with multiple AR.Drawables. The AR.Drawables can be defined for multiple targets. A target can be the camera, the radar or a direction indicator. Both the radar and direction indicators will be covered in more detail in later examples.
    */

    this.poiData = poiData;

    // create the AR.GeoLocation from the poi data
    var markerLocation = new AR.GeoLocation(poiData.latitude, poiData.longitude, poiData.altitude);

    // create an AR.ImageDrawable for the marker in idle state
    var imgTemp = new AR.ImageResource(poiData.immagineIndicatore);
    //var overlay = new AR.ImageDrawable(imgTemp, poiData.dimensioneIndicatore,

    this.arrayOggetti = new Array();

    this.radarCircle = new AR.Circle(0.05, {
        horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.CENTER,
        opacity: 0.8,
        style: {
            fillColor: "#0000FF"
        }
    });

    this.radardrawables = [];
    this.radardrawables.push(this.radarCircle);

    var overlay = new AR.ImageDrawable(imgTemp, poiData.dimensioneIndicatore,
    {
        enabled: true,
        opacity : 1.0,
        zOrder: 10
    });
    overlay.onClick = Marker.prototype.mostraContenuti(poiData.id);
    overlay.customType=-1;

    this.arrayOggetti.push(overlay);
    /*
    this.markerDrawable_idle = new AR.ImageDrawable(World.markerDrawable_idle, 2.5, {
        zOrder: 0,
        opacity: 1.0,
        onClick: Marker.prototype.getOnClickTrigger(this)
    });
*/

/*
    // create an AR.ImageDrawable for the marker in selected state
    this.markerDrawable_selected = new AR.ImageDrawable(World.markerDrawable_selected, 2.5, {
        zOrder: 0,
        opacity: 0.0,
        onClick: null
    });
*/
    var labels = new Array();
    labels.push(poiData.label);

    if (poiData.labelAddizionale==1) //distanza
    {
        var distanzaStr="";
        if (poiData.realDist<1) distanzaStr=parseInt(poiData.realDist*1000)+" "+getLabel("METRI","it","m");
        else distanzaStr=poiData.realDist.toFixed(1)+" Km";
        distanzaStr = getLabel("DISTANZA","it","m")+": " + distanzaStr;
        labels.push(distanzaStr);
    }
    else if (poiData.labelAddizionale==2) //coordinate
    {
        var coordinateStr="Lat: " + parseFloat(poiData.latitude).toFixed(4) + " - Lon: " + parseFloat(poiData.longitude).toFixed(4);
        labels.push(coordinateStr);
    }
    else if (poiData.labelAddizionale==3) //altitudine
    {
        var altitudeStr=getLabel("ALTITUDINE","it","m")+": " + poiData.realAlt + " " + getLabel("METRI","it","m");
        labels.push(altitudeStr);
    }



    for (var i=0;i<labels.length;i++)
    {
        var altezzaLabel=0.5;
        var myFontstyle=AR.CONST.FONT_STYLE.BOLD;
        var labelText=labels[i];
        if (i>0)
        {
            altezzaLabel=0.4;
            myFontstyle=AR.CONST.FONT_STYLE.ITALIC;
            labelText=labels[i]+" "; //per l'italico
        }
        var label = new AR.Label(labelText, altezzaLabel, {
            scale: 1,
            enabled: false,
            opacity : 1,
            zOrder: 16,
            style : {
                textColor : "#000000",
                //backgroundColor : '#FFFFFFBB',
                fontStyle:myFontstyle
            }
        });
        label.onClick = Marker.prototype.mostraContenuti(poiData.id);
        label.customType=-1;

        var offsetGeneraleInizialeX=0;
        var offsetGeneraleInizialeY=0;

        if (poiData.labelPosition=="up")
        {
            // a seconda dell'indice lo devo mettere pi� o meno sopra
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX;
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6)+(0.6*(labels.length-i-1));
        }
        else if (poiData.labelPosition=="down")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX;
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6)-(0.6*i);
        }
        else if (poiData.labelPosition=="sx")
        {
            var displY=0;
            if (labels.length % 2 == 0) //pari
            {
              var centralElement=parseInt(labels.length/2); 
              displY=0.6*(labels.length-1-centralElement-i)+0.3;
            }
            else
            {
              var centralElement=parseInt(labels.length/2); 
              displY=0.6*(labels.length-1-centralElement-i);
            }
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+displY;
        }
        else if (poiData.labelPosition=="dx")
        {
            var displY=0;
            if (labels.length % 2 == 0) //pari
            {
              var centralElement=parseInt(labels.length/2); 
              displY=0.6*(labels.length-1-centralElement-i)+0.3;
            }
            else
            {
              var centralElement=parseInt(labels.length/2); 
              displY=0.6*(labels.length-1-centralElement-i);
            }
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+displY;
        }
        else if (poiData.labelPosition=="upDx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6)+(0.6*(labels.length-i-1));
        }
        else if (poiData.labelPosition=="downDx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6)-(0.6*i);
        }
        else if (poiData.labelPosition=="downSx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6)-(0.6*i);
        }
        else if (poiData.labelPosition=="upSx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6)+(0.6*(labels.length-i-1));
        }
        else //di default a destra
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY;
        }
        this.arrayOggetti.push(label); 
    }


    for (var i=0;i<labels.length;i++)
    {
        var altezzaLabel=0.5;
        var myFontstyle=AR.CONST.FONT_STYLE.BOLD;
        var labelText=labels[i];
        if (i>0)
        {
            altezzaLabel=0.4;
            myFontstyle=AR.CONST.FONT_STYLE.ITALIC;
            labelText=labels[i]+" "; //per l'italico
        }
        var label = new AR.Label(labelText, altezzaLabel, {
            scale: 1,
            enabled: false,
            opacity : 1,
            zOrder: 15,
            style : {
                textColor : "#aaaaaa",
                //backgroundColor : '#FFFFFFBB',
                fontStyle:myFontstyle
            }
        });
        label.onClick = Marker.prototype.mostraContenuti(poiData.id);
        label.customType=-1;

        var offsetGeneraleInizialeX=0;
        var offsetGeneraleInizialeY=0;

        if (poiData.labelPosition=="up")
        {
            // a seconda dell'indice lo devo mettere pi� o meno sopra
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX;
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6)+(0.6*(labels.length-i-1));
        }
        else if (poiData.labelPosition=="down")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX;
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6)-(0.6*i);
        }
        else if (poiData.labelPosition=="sx")
        {
            var displY=0;
            if (labels.length % 2 == 0) //pari
            {
              var centralElement=parseInt(labels.length/2); 
              displY=0.6*(labels.length-1-centralElement-i)+0.3;
            }
            else
            {
              var centralElement=parseInt(labels.length/2); 
              displY=0.6*(labels.length-1-centralElement-i);
            }
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+displY;
        }
        else if (poiData.labelPosition=="dx")
        {
            var displY=0;
            if (labels.length % 2 == 0) //pari
            {
              var centralElement=parseInt(labels.length/2); 
              displY=0.6*(labels.length-1-centralElement-i)+0.3;
            }
            else
            {
              var centralElement=parseInt(labels.length/2); 
              displY=0.6*(labels.length-1-centralElement-i);
            }
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+displY;
        }
        else if (poiData.labelPosition=="upDx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6)+(0.6*(labels.length-i-1));
        }
        else if (poiData.labelPosition=="downDx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6)-(0.6*i);
        }
        else if (poiData.labelPosition=="downSx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6)-(0.6*i);
        }
        else if (poiData.labelPosition=="upSx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6)+(0.6*(labels.length-i-1));
        }
        else //di default a destra
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY;
        }
        label.translate.x=label.translate.x+0.03;
        label.translate.y=label.translate.y-0.03;
        this.arrayOggetti.push(label); 
    }
    
/*

    var myHtml="<div style='border:1px solid red;padding:10px;font-size:25px;background-color:#ffffff;'>";
    for (var i=0;i<labels.length;i++)
    {
        myHtml+=labels[i]
        if (i!=(labels.length-1)) myHtml+="<br>";
    }
    myHtml+="</div>";

    var vpWidth=100;
    //var vpHeight=(25*labels.length)+20+2;
    var vpHeight=50;
   


    var label = new AR.HtmlDrawable({html:myHtml}, 3, {
        viewportWidth: vpWidth,
        viewportHeight: vpHeight,
        backgroundColor: "#FF0000",
        //translate: {
        //    x:0.36,
        //    y: 0.5
        //},
        //horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.RIGHT,
        //verticalAnchor: AR.CONST.VERTICAL_ANCHOR.TOP,
        clickThroughEnabled: true,
        allowDocumentLocationChanges: false
    });

   
        var offsetGeneraleInizialeX=0;
        var offsetGeneraleInizialeY=0;

        if (poiData.labelPosition=="up")
        {
            // a seconda dell'indice lo devo mettere pi� o meno sopra
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX;
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="down")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX;
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="sx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY;
        }
        else if (poiData.labelPosition=="dx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY;
        }
        else if (poiData.labelPosition=="upDx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="downDx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="downSx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="upSx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6);
        }
        else //di default a destra
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY;
        }
        this.arrayOggetti.push(label); 
*/


    // create the AR.GeoObject with the drawable objects
    var self=this;
    this.markerObject = new AR.GeoObject(markerLocation, {
        onEnterFieldOfVision : function(){
            console.log("Sto vedendo: " + self.poiData.label);
            
            for (var i=0;i<labels.length;i++)
            {
                self.arrayOggetti[i+1].scale=0;
                self.arrayOggetti[i+1].enabled=true;
                self.arrayOggetti[i+1+labels.length].scale=1;
                self.arrayOggetti[i+1+labels.length].opacity=0;
                self.arrayOggetti[i+1+labels.length].enabled=true;

                var animation1 = new AR.PropertyAnimation(self.arrayOggetti[i+1], "scale", 0, 1, 300*(i+1), AR.CONST.EASING_CURVE_TYPE.EASE_OUT_BACK);
                var animation2 = new AR.PropertyAnimation(self.arrayOggetti[i+1+labels.length], "opacity", 0, 0.9, 50, AR.CONST.EASING_CURVE_TYPE.EASE_OUT_BACK);
                var animationGroup = new AR.AnimationGroup(
                  AR.CONST.ANIMATION_GROUP_TYPE.SEQUENTIAL, // the animations will run in parallel
                  [animation1, animation2]);
                animationGroup.start();
            }
            
        },
        //the function executed when the GeoObject exits the field of vision
        onExitFieldOfVision : function(){
            console.log("ho perso: " + self.poiData.label);
            //var animation = new AR.PropertyAnimation(self.arrayOggetti[1], "scale", 1, 0, 1000, AR.CONST.EASING_CURVE_TYPE.EASE_IN_BACK, {}, {onFinish : function() { self.arrayOggetti[1].enabled=false; }});
            //animation.start();
            
            for (var i=1;i<self.arrayOggetti.length;i++)
            {
                self.arrayOggetti[i].scale=0;
                self.arrayOggetti[i].enabled=false;
            }
            
        },
        drawables: {
            cam: self.arrayOggetti,
            radar: this.radardrawables
        }
    });
    /*
    this.markerObject = new AR.GeoObject(markerLocation, {
        drawables: {
            cam: [overlay, label]
        }
    });
    */
    

    return this;
}


Marker.prototype.mostraContenuti = function (idPoi)
{
    return function() {
        console.log("sono in mostraContenuti, con indicePoi: "+idPoi);
        //console.log("dati: " +JSON.stringify(oggetto.target[indiceImmagine].punti[indiceElemento]));
        showContenuto(idPoi);

    };
};


Marker.prototype.getOnClickTrigger = function(marker) {

    /*
        The setSelected and setDeselected functions are prototype Marker functions.

        Both functions perform the same steps but inverted, hence only one function (setSelected) is covered in detail. Three steps are necessary to select the marker. First the state will be set appropriately. Second the background drawable will be enabled and the standard background disabled. This is done by setting the opacity property to 1.0 for the visible state and to 0.0 for an invisible state. Third the onClick function is set only for the background drawable of the selected marker.
    */

    return function() {

        if (marker.isSelected) {

            Marker.prototype.setDeselected(marker);

        } else {
            Marker.prototype.setSelected(marker);
            try {
                World.onMarkerSelected(marker);
            } catch (err) {
                alert(err);
            }

        }

        return true;
    };
};

Marker.prototype.setSelected = function(marker) {

    marker.isSelected = true;

    marker.markerDrawable_idle.opacity = 0.0;
    marker.markerDrawable_selected.opacity = 1.0;
    marker.markerDrawable_idle.onClick = null;
    marker.markerDrawable_selected.onClick = Marker.prototype.getOnClickTrigger(marker);
};

Marker.prototype.setDeselected = function(marker) {

    marker.isSelected = false;

    marker.markerDrawable_idle.opacity = 1.0;
    marker.markerDrawable_selected.opacity = 0.0;

    marker.markerDrawable_idle.onClick = Marker.prototype.getOnClickTrigger(marker);
    marker.markerDrawable_selected.onClick = null;
};

// will truncate all strings longer than given max-length "n". e.g. "foobar".trunc(3) -> "foo..."
String.prototype.trunc = function(n) {
    return this.substr(0, n - 1) + (this.length > n ? '...' : '');
};